<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    
    
    //=============  HomePage
   'our-products' => 'ផលិតផលរបស់យើង ',
    'view-detail' => 'មើលពត៌មានលំអិត ',
    'our-company' => 'ក្រុមហ៊ុន​របស់​យើង',
    'lylyfood' => 'ក្រុមហ៊ុនលីលីហ្វូតលីមីតធីតខូអិលធីឌី',
    'phnom-penh-city' => 'ទីក្រុងភ្នំពេញនិងខេត្តផ្សេងៗទៀតក្នុងប្រទេសកម្ពុជាដើម្បីបង្ហាញផលិតផលលីលីជាផលិតផលកម្ពុជាកូរ៉េជប៉ុនចិនម៉ាឡេស៊ីថៃវៀតណាម។ ',
    'more' => 'ច្រើនទៀត',
    'like-lyly' => 'អាហារលីលីជាអាហារដ៏ល្អបំផុតនៅក្នុងប្រទេសកម្ពុជានិងនៅជិតព្រំដែន។',
    
    'award' => 'ពានរង្វាន់',
    //=============  Product
    'product-detail' => 'ព័ត៌មានលំអិតអំពីផលិតផល',
    'name' => 'ឈ្មោះ',
    'category' => 'ប្រភេទ',
    'year' => 'ឆ្នាំផលិត',
    'ingredient' => 'គ្រឿងផ្សំ',
    //=============  News
    'news-detail' => 'ព័ត៌មានលម្អិត',
    'related-news' => 'ព័ត៌មានទាក់ទង',
    //=============  Gallery
    'picture'      => 'រូបភាព',
    'gallery-detail' => 'រូបភាពលម្អិត',
    'video'      => 'វីដេអូពី',
    'video-detail'      => 'វីដេអូពី Youtube',
    //=============  About-us
    'partner'   => 'ដៃគូរសហាការ',
    'job'   => 'ការងារ',
    'description'   => 'ការពិពណ៌នា',
    'requirments'   => 'តម្រូវការ',
    'date-closed'   => 'កាលបរិច្ឆេទបិទ',
    'location'   => 'ទីកន្លែង',
    'job-title'   => 'ចំណងជើងការងារ',
    'salary'   => 'ប្រាក់ខែ',
    'submit-cv'   => 'ផ្ញើ CV របស់អ្នកតាមអ៊ីម៉ែល',
    //=============  Contact-us
    'have-any-question'   => 'មានសំណួរណាមួយទេ?',
    'call-us'   => 'ហៅទូរស័ព្ទមកយើង',
    'addresss'   => 'អាសយដ្ឋាន',
    'interested-in-discussing?'   => 'ចាប់អារម្មណ៍ក្នុងការពិភាក្សា?',
    'send-your-message'   => 'ផ្ញើសាររបស់អ្នក',

];
