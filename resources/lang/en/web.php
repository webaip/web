<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    
    
    //=============  HomePage
   
    'our-products' => 'Our Products ',
    'view-detail' => 'View Detail ',
    'our-company' => 'Our Company',
    'lylyfood' => 'LY LY FOOD INDUSTRY CO., LTD ',
    'phnom-penh-city' => 'Phnom Penh city and other provines in Cambodia to showcase Lyly products as a Cambodian product Korea Japan China Malaysia Thailand Vietnam. ',
    'more' => 'more',
    'like-lyly' => 'LYLY FOOD, THE BEST SNAK CAKE IN CAMBODIA AND BEYON BORDER.',
    'more' => 'more',
    'award' => 'Award',
    //=============  Product
    'product-detail' => 'Product Detail',
    'name' => 'Name',
    'category' => 'Category',
    'year' => 'Year',
    'ingredient' => 'Ingredient',
    //=============  News
    'news-detail' => 'News Detail',
    'related-news' => 'Related News',
    //=============  Gallery
    'picture'      => 'Picture',
    'gallery-detail' => 'Picture Detail',
    'video'      => 'Video',
    'video-detail'      => 'Video From Youtube',
    //=============  About-us
    'partner'   => 'Partner',
    'job'   => 'Job',
    'description'   => 'Description',
    'requirments'   => 'Requirments',
    'date-closed'   => 'Date Closed',
    'location'   => 'Location',
    'job-title'   => 'Job Title',
    'salary'   => 'Salary',
    'submit-cv'   => 'Submit your CV by email',
    //=============  Contact-us
    'have-any-question'   => 'Have any Questions?',
    'call-us'   => 'Call Us',
    'addresss'   => 'Address',
    'interested-in-discussing'   => 'Interested in discussing?',
    'send-your-message'   => 'Send your message',
];