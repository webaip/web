@extends('cp.banner.tabForm')
@section ('section-title', "Slide")
@section ('tab-active-system-permision', 'active')
@section ('tab-css')

@endsection

@section ('tab-js')
<script type="text/javascript">
	$(document).ready(function(){
		$('.item').click(function(){
			check_id = $(this).attr('for');
			page_id = $("#"+check_id).attr('page-id');
			features(page_id);
		})
	})
	function features(page_id){
		$.ajax({
		        url: "{{ route($route.'.check-page') }}?banner_id={{ $id }}&page_id="+page_id,
		        type: 'GET',
		        data: { },
		        success: function( response ) {
		            if ( response.status === 'success' ) {
		            	toastr.success(response.msg);
		            }else{
		            	swal("Error!", "Sorry there is an error happens. " ,"error");
		            }
		        },
		        error: function( response ) {
		           swal("Error!", "Sorry there is an error happens. " ,"error");
		        }
		});
	}
</script>

@endsection

@section ('tab-content')
	<br />
		
		
		
		<div class="row m-t-lg">
			@foreach( $pages as $page )
				@php( $check = "" )
		        @foreach($data as $row)
		            @if($row->page_id == $page->id)
		                @php( $check = "checked" )
		            @endif
		        @endforeach
				<div class="col-sm-6 col-sm-4 col-md-3 col-lg-3">
					<div class="checkbox-bird">
						<input type="checkbox" page-id="{{ $page->id }}" id="page-{{ $page->id }}" {{ $check }}>
						<label class="item" for="page-{{ $page->id }}">{{ $page->name }}</label>
					</div>
				</div>
			@endforeach
		</div>
		<hr />

@endsection