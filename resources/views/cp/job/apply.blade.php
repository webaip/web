@extends($route.'.tab')
@section ('section-title', 'Applyer')
@section ('tab-active-adviser', 'active')
@section ('section-css')
	
@endsection



@section ('section-js')


	

	
@endsection

@section ('tab-content')
	
	@if(sizeof($data) > 0)
<div class="table-responsive">
	<table id="table-edit" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>#</th>
				<th>Full Name</th>
				<th>Sex</th>
				<th>Phone</th>
				<th>Email</th>
				<th>CV</th>
			</tr>
		</thead>
		<tbody>

			@php ($i = 1)
			@foreach ($data as $row)
				<tr>
					<td>{{ $i++ }}</td>
					<td>{{ $row->name }}</td>
					<td>{{ $row->sex }}</td>
					<td>{{ $row->phone }}</td>
					<td>{{ $row->email }}</td>

					<td style="white-space: nowrap; width: 1%;">
						<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
                           	<div class="btn-group btn-group-sm" style="float: none;">
                           		<a href="{{ asset($row->cv) }}" class="tabledit-edit-button btn btn-sm btn-success" style="float: none;"><span class="fa fa-eye">Download CV</span></a>
                           	</div>
                       </div>
                    </td>
				</tr>
			
			@endforeach
			
			
		</tbody>
	</table>
</div >
@else
	<span>No Data</span>
@endif
@endsection