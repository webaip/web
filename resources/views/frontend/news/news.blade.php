@extends('frontend.layouts.master')

@section('title', 'News | '.env('APP_NAME'))

@section(last(request()->segments()), 'active')

@section ('content')
@php($banner = getBanner(5))
    @if($banner)
        @include('frontend.layouts.banner', ['banner'=>['image'=>asset($banner->banner->image), 'title'=>'News']])
    @endif 
    <section class="services-page-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-5 col-sm-7 col-xs-12 pull-left">
                
                @include('frontend.news.menu')
                
            </div>  
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 pull-right">
                @if($data)
                <div class="service-content">
                @php($route = last(request()->segments()))
                  
                    @foreach($data->chunk(3) as $item)
                    <div class="row">
                        <!--Start single item-->
                        @foreach($item as $row)
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-blog-post">
                                <div class="img-holder">
                                    <img src="{{ asset($row->feature_image) }}" alt="Awesome Image">
                                    <div class="overlay-style-one">
                                        <div class="box">
                                            <div class="content">
                                                <a href="{{ route('news-detail',['biz'=>$route,'slug'=>$row->slug]) }}"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-holder">
                                    <ul class="meta-info">
                                        <li><a href="{{ route('news-detail',['biz'=>$route,'slug'=>$row->slug]) }}">{{ Carbon\Carbon::parse($row->created_at)->format('d') }}-{{ Carbon\Carbon::parse($row->created_at)->format('F') }}-{{ Carbon\Carbon::parse($row->created_at)->format('Y') }}</a></li>
                                    </ul>
                                    <a href="{{ route('news-detail',['biz'=>$route,'slug'=>$row->slug]) }}">
                                        <h3 class="blog-title">{{$row->title}}</h3>
                                    </a> 
                                    <div class="text">
                                        <p>{{str_limit($row->description,90)}}</p>
                                    </div>
                                    <div class="bottom clearfix">
                                        <div class="readmore pull-left">
                                            
                                        </div>
                                        <div class="comment pull-right">
                                            <a href="{{ route('news-detail',['biz'=>$route,'slug'=>$row->slug]) }}">Read More</a> 
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div>

                        
                        @endforeach
                        <!--End single item-->
                        
                    </div>
                    @endforeach
                </div>  
                <div class="row">
                    <div class="col-md-12">
                        {{ $data->links('vendor.pagination.frontend-html') }}
                    </div>
                </div>  
                @else
                    <p>No Data</p>
                @endif
            </div>
                
        </div>
    </div>
</section>





@endsection