@extends('frontend.layouts.master')

@section('title', 'Projects | '.env('APP_NAME'))
@section('active-projects', 'current')
@section(request()->segments()['1'], 'active')

@section ('content')
@php($banner = getBanner(6))
  @if($banner)
        @include('frontend.layouts.banner', ['banner'=>['image'=>asset($banner->banner->image), 'title'=>'News Detail']])
    @endif 
    <section class="services-page-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-5 col-sm-7 col-xs-12 pull-left">
                
                @include('frontend.news.menu')
                
            </div>  
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 pull-right">
                <div class="service-content">
                    <div class="row">
                       
                    <!--Start top content-->
                    <div class="row top-content">
                        <!--Start single item-->
                        <div class="col-md-12">
                            <div class="blog-post">
                                <div class="single-blog-post">
                                        <div class="img-holder">
                                        @if($data) <img src="{{ asset($data->image) }} "  alt="Awesome Image">@endif
                                        </div>
                                        
                                        <div class="text-holder">
                                        <ul class="meta-info">
                                            <li>
                                                <a href="#"> 
                                                @if($data)
                                                {{ Carbon\Carbon::parse($data->closed_date)->format('d') }}-{{ Carbon\Carbon::parse($data->closed_date)->format('F') }}-{{ Carbon\Carbon::parse($data->closed_date)->format('Y') }}
                                                @endif
                                            </a>
                                            </li>
                                        </ul>
                                        <h3 class="blog-title">@if($data) {{$data->title}} @endif</h3>
                                        <br>
                                        <p>@if($data) {!!$data->content!!} @endif</p>
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <!--End single item-->
                    </div>
                    <!--End top content-->
                        
                    </div>
                </div>    
            </div>
                
        </div>
    </div>
</section>





@endsection