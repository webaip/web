<div class="left-sidebar">
    <!--Start single sidebar-->
    <div class="single-sidebar">
        <ul class="page-link">
            <li><a class="@yield('active-atp-consult')"  href="{{ route('our-business', ['biz'=>'atp-consultant']) }}">Asean Tourism Professional Consultant</a></li>
            <li><a class="@yield('active-school')" href="{{ route('our-business', ['biz'=>'atp-school-of-hospitality-and-tourism']) }}">School of Hospitality and Tourism</a></li>
            <li><a class="@yield('active-green-cafe')" href="{{ route('our-business', ['biz'=>'green-cafe-and-restaurant']) }}">Green Cafe and Restaurant</a></li>   
        </ul>
    </div>
    <!--End single sidebar-->
    
    
    
    <!--Start single sidebar-->
    <div class="single-sidebar">
        <ul class="brochures-dwn-link">
            <li>
                <a href="{{asset(getCompanyProfile())}}">
                    <div class="icon-holder">
                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i>  
                    </div>
                    <div class="title-holder">
                        <h5>Download Company Profile</h5>    
                    </div>
                </a>
            </li>
            
        </ul> 
    </div>
    <!--End single sidebar-->
    
</div>