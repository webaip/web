
@extends('frontend.layouts.master')

@section('title', 'Our Business | '.env('APP_NAME'))
@section('active-our-businesses', 'current')
@section('active-atp-consult', 'active')


@section ('content')
@php($banner = getBanner(2))
    @if($banner)
        @include('frontend.layouts.banner', ['banner'=>['image'=>asset($banner->banner->image), 'title'=>'ASEAN Tourism Professional Consultant']])
    @endif 

  
    <!--Start Single service area-->
<section id="single-service-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-5 col-sm-7 col-xs-12 pull-left">
                
            @include('frontend.our-bussinesses.menu')

            </div>  
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 pull-right">
                <div class="content-box">
                    <!--Start top content-->
                    <div class="row top-content">
                        <!--Start single item-->
                        <div class="col-md-12">
                            <div class="single-item">
                                <div class="img-holder">
                                    <img src="{{ asset($data->banner) }}" alt="Awesome Image">
                                </div>
                                {!! $data->content !!}
                                <!-- <div class="text-holder">
                                    <h3>About Us</h3>
                                    <p class="mar-btm15">ATP Consultant Co., Ltd was established in 2015 by a number of outstanding Cambodian ASEAN master trainers and assessors who acquire both experiences and qualification in the field of hospitality training. ATP Consultant is the abbreviation of ASEAN Tourism Professionals Consultant. ATP Consultant is strongly recognized by the Cambodia Ministry of Tourism, National Committee for Tourism Professional and ASEAN Member States for its outstanding work within Mutual Recognition Arrangement on Tourism Professionals (MRA-TP) and ASEAN Qualification Reference Framework (AQRF).</p>

                                    <p class="mar-btm15"> Each Board of Director of the ATP Consultant has more than 10 years of experience in the tourism and hospitality industry, and more than five year experiences in implementing the ASEAN Common Competency Standard for Tourism Professionals (ACCSTP) under the ASEAN MRA-TP framework. ATP Consultant team has been working closely with the NCTP to provide the National Master Trainers and Master Assessors training in housekeeping, food and beverage services, and front office division. The team also facilitated the pilot project of Recognition of Prior Learning Assessment for PSE in 2015. </p>

                                    <p class="mar-btm15"> ATP Consultant commits to provide high quality products and services which align with vision and strategic action of the Cambodia Ministry of Tourism and the National Committee for Tourism Professionals in order to promote the implementation of ASEAN MRA-TP for the successful of human resource development in tourism and hospitality sector.  
									 </p>
                                    
                                    <h3>Our Vision</h3>
                                    <p class="mar-btm15">
                                        Our vision is to become a leader in skill training, assessment and consulting on the basis of the ASEAN Mutual Recognition Arrangement on Tourism Professionals (MRA-TP) for hospitality and tourism related businesses.
                                    </p>

                                    <h3>Mission</h3>
                                    <ul>
                                        <li> - To develop human resources in tourism and hospitality industry focusing on hotel and travel service on the basis of ASEAN Mutual Recognition Arrangement on Tourism Professionals. </li>
                                        <li> - To provide professional consulting with ethic and quality of services</li>
                                    </ul>


                                     <h3>Objective</h3>
                                    <p class="mar-btm15">
                                       <p>
                                           <ul>
                                               <li> - To promote human resource development in tourism and hospitality sector on the basis of   ASEAN Mutual Recognition Arrangement on Tourism Professionals.</li>
                                               <li> - To work with the Cambodia Ministry of Tourism to achieve its strategic plan in human resource development in tourism and hospitality industry.</li>
                                               <li> - To provide assistance to our partners to obtain qualification, skill passport and certificate of competence which are recognised by National Committee for Tourism Professionals.  </li>
                                           </ul>
                                       </p>

                                       <br />
                                      
                                    </p>
                                </div>

                                 <div class="text-holder">
                                    <h3>Products & Service</h3>
                                    <p class="mar-btm15">ATP Consultant provides three main products and service as following: </p>
                                    <h5><b>A- Consulting service</b></h5>
                                    <p class="mar-btm15">
                                    	We provide a variety of consulting services  ranging from developing business concept to training programme and  standard of operation procedure for both hospitality school and businesses: 
                                    	<ol>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Develop business concept for hospitality and tourism schools, hotels, restaurants, café shops and catering operations. 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Develop training programme for hospitality and tourism schools. 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Develop curriculums for hotel and travel service 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Develop staff capacity building program 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Develop standard of operation procedure (SOP) 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Training Needs Analysis (TNA)
                                    		</li>
                                    	</ol>
                                    </p>
                                    <h5><b>C- Assessment service</b></h5>
                                    <p class="mar-btm15">
                                    	<ol>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Competency – Based Assessment (CBA) under MRA-TP Framework
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> In-house training for tourism and hospitality related businesses  
                                    		</li>
                                    		
                                    	</ol>
                                    </p>
                                     <h5><b>B- Training service</b></h5>
                                    <p class="mar-btm15">
                                    	<ol>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Competency - Based Training (CBT) in hotel and travel service using the Common ASEAN Tourism Curriulum (CATC)
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Facilitate recognition of skill passport 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Conduct and facilitate the Recogniton of Prior Leaning (RPL) and the Recognition of Current Competencies (RCC) 
                                    		</li>
                                    		
                                    	</ol>
                                    </p>
                                </div>

                                <div class="text-holder">
                                    <h3>Porfolio</h3>
                                    <h5><b>Skill Development Program (SDP) – SwissContact Cambodia [yyyy – yyyy] </b></h5>
                                    <p class="mar-btm15">
                                    	<ol>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Develop curricula in the divisions of F&B service, housekeeping, and front office for both low-skill workers and owners/managers 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Develop in-house training material for National Trainer and National Assessor in the divisions of F&B service, housekeeping, and front office
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Develop Recognition of Prior Learning (RPL) mechanism and RPL manual for the National Committee for Tourism Professionals (NCTP) and SDP. 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Conduct skill test assessment for in-house training programme within 3 provinces of Kratie, Stung Treng and Preah Vihear 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Deliver training to hospitality enterprise owners/managers and officers of the provincial department of tourism 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Deliver training to low-skill workers on F&B Service, front office and housekeeping
                                    		</li>
                                    	</ol>
                                    </p>

                                    <h5><b>Mekong Inclusive Growth and Innovation Programme (MIGIP) – SwissContact Cambodia [yyyy – yyyy]  </b></h5>
                                    <p class="mar-btm15">
                                    	<ol>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Develop training materials for National trainer in F&B service Division
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Conduct Train of Trainer (ToT) training and assessment for national trainers 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Conduct an assessment study with owners/managers of hospitality enterprises in Kampot 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Provide coaching to national trainers to deliver effective training
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Conduct Skill test Assessment to 40 candidate at Kampot based MIGIP projection and issue certificate of completion
                                    		</li>
                                    	</ol>
                                    </p>

                                    <h5><b>Barista RPL Assessment – National Committee for Tourism Professionals  [yyyy – yyyy]   </b></h5>
                                    <p class="mar-btm15">
                                    	<ol>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Provide consultation service on curricula and assessment material development
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Conduct RPL assessment to baristas from various café in Phnom Penh area.
                                    		</li>
                                    		
                                    	</ol>
                                    </p>

                                    <h5><b>Establishing Tourism Trainer Club (TTC)   </b></h5>
                                    <p class="mar-btm15">
                                    	<ol>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Gathering tourism professionals from various enterprises
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Facilitate weekly training for TTC on the basis of ASEAN Tourism Reference Qualification Framework
                                    		</li>
                                    		
                                    	</ol>
                                    </p>

                                </div> -->
                            </div>
                        </div>
                        <!--End single item-->
                    </div>
                    <!--End top content-->




                    <!--Start Benefits Service content-->
                    <!-- <div class="benefits-service-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="sec-title">
                                    <h3>Key Benefits of the Service</h3>
                                    <span class="border"></span>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-12">
                                        <div class="text-holder">
                                            <p>Beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue beguiled and demoralized.</p> 
                                            <ul>
                                                <li><i class="fa fa-angle-right" aria-hidden="true"></i>Develop new ideas and market them</li>
                                                <li><i class="fa fa-angle-right" aria-hidden="true"></i>Build leadership and management skills</li>
                                                <li><i class="fa fa-angle-right" aria-hidden="true"></i>Improve manufacturing processes</li>
                                                <li><i class="fa fa-angle-right" aria-hidden="true"></i>Build a business strategy and plan</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12">
                                        <div class="video-holder">
                                            <img src="{{ asset('public/frontend/images/services/service-single/video-gallery.jpg') }}" alt="Awesome Video Gallery">
                                            <div class="overlay-gallery">
                                                <div class="icon-holder">
                                                    <div class="icon">
                                                        <a class="html5lightbox" title="Consultive Video Gallery" href="https://www.youtube.com/watch?v=-VW_tLcGgwA"><img src="{{ asset('public/frontend/images/icon/play-btn.png') }}" alt="Play Button"></a>   
                                                    </div>
                                                </div>    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    
                </div>
            </div>
            
            
            
        </div>
    </div>
</section>
<!--End Single service area-->





@endsection