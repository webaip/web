@extends('frontend.layouts.master')

@section('title', 'Our Business | '.env('APP_NAME'))
@section('active-our-businesses', 'current')
@section('active-school', 'active')

@section ('content')
@php($banner = getBanner(3))
    @if($banner)
        @include('frontend.layouts.banner', ['banner'=>['image'=>asset($banner->banner->image), 'title'=>'School of Hospitality and Tourism']])
    @endif 
   <!--Start Single service area-->
<section id="single-service-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-5 col-sm-7 col-xs-12 pull-left">
                @include('frontend.our-bussinesses.menu')
            </div>  
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 pull-right">
                <div class="content-box">
                    <!--Start top content-->
                    <div class="row top-content">
                        <!--Start single item-->
                        <div class="col-md-12">
                            <div class="single-item">
                                <div class="img-holder">
                                    <img src="{{ asset($data->banner) }}" alt="Awesome Image">
                                </div>
                                {!! $data->content !!}
                                <!-- <div class="text-holder">
                                    <h3>About Us</h3>
                                    <p class="mar-btm15">We are proud to be amongst the first technical and vocational education and training schools in Cambodia to launch the ASEAN Mutual Recognition Arrangement for Tourism Professionals study programme. ATP School of Hospitality and Tourism (ATP-SoHT) was established in November 2017 by a number of tourism and hospitality professionals who acquire years of experience in the industry. Our aim is to provide the regionally recognized hospitality training programme that accommodates the needs of tourism job market in Cambodia and in ASEAN region. Our curricula are developed on the basis of the Common ASEAN Tourism Curriculum (CATC) which is the integral part of the ASEAN Mutual Recognition Arrangement on Tourism Professionals (MRA-TP). In March 2018 our curricula were endorsed by the National Committee for Tourism Professionals of the Ministry of Tourism.</p>
                                    <p class="mar-btm15">

                                       ATP-SoHT equips with all necessary training material and equipment that help students build their competencies including skill, knowledge and attitude to be tourism professionals. In this regard, ATP-SoHT own three classrooms, an application Green Café and Restaurant, a reception desk, a library and computer lab, a laundry room and a sample hotel room. Moreover, ATP-SoHT builds a strong network with various hospitality businesses, schools, NGO and governmental agencies to expand internship and job opportunities for our students.  
                                        
                                    </p>
                                    
                                    <h3>Our Vision</h3>
                                    <p class="mar-btm15">
                                        Our vision is to become the school of excellence in delivering quality of human resources for the fast growing hospitality and tourism industry in Cambodia. 
                                    </p>

                                    <h3>Mission</h3>
                                    <ul>
                                        <li> - To prepare students for challenging careers in the hospitality and tourism sectors.  </li>
                                        <li> - To provide a hospitality training programme that addresses the needs national and regional hospitality and tourism industry.</li>
                                    </ul>
                                    <br>
                                    <h3>Objective</h3>
                                    <p class="mar-btm15">
                                       <p>
                                           <ul>
                                               <li> - Develop high quality professionals for tourism and hospitality industry in Cambodia</li>
                                               <li> - Provide hospitality and tourism education and training programme that aligns with national and regional standards.</li>
                                               <li> - Provide a training programme that addresses the ever changing industry trends. </li>
                                               <li> - Offer equal opportunities and create an equitable work environment for all youth living in different parts of the country, while ensuring sustainable growth in the hospitality and tourism industry.</li>
                                           </ul>
                                       </p>

                                    </p>
                                    <br>
                                    <div class="text-holder">
                                        <h3>Study Programme and Qualification</h3>
                                        <br>
                                        <h5><b>Higher Diploma in Hotel and Tourism Management  </b></h5>
                                        <p>
                                            The higher diploma in hotel and tourism management provided by ATP-SoHT is a qualification based vocational education programme which equals to level 5 in ASEAN Qualification Reference Framework and Associate Degree in Cambodian Higher Education.    It is a two year vocational education programme combining theoretical knowledge and practical skills.
                                        </p>
                                        <p class="mar-btm15">
                                         <table class="table table-bordered">
                                            <thead>
                                              <tr>
                                                <th>Year</th>
                                                <th>Course</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                              <tr>
                                                <td>Foundation</td>
                                                <td>
                                                    <p>
                                                        General Tourism and Hospitality Knowledge 
                                                    </p>
                                                    <p>
                                                        Understanding and Providing Food and Beverage (F&B) Service 
                                                    </p>
                                                    <p>
                                                        Understanding and Providing Front Office (FO) Service 
                                                    </p>
                                                    <p>
                                                        Understanding and Providing Housekeeping Service  (HK)
                                                    </p>
                                                    <p>
                                                        3 month Internship (Compulsory)
                                                    </p>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>Year 2</td>
                                                <td>
                                                    <p>
                                                        Principle of Hotel and Tourism Management
                                                    </p>
                                                    <p>
                                                        Management in F&B, FO and HK
                                                    </p>
                                                    <p>
                                                        6 month internship (Compulsory)
                                                    </p>
                                                </td>
                                              </tr>
                                              
                                            </tbody>
                                        </table>
                                        <h5><i>Vocational Certificate [to be developed]   </i></h5>
                                            <ol>
                                                <li>
                                                    Year 1: 540 (per year) or 270 (per semester)
                                                </li>
                                                <li>
                                                    Year 2: 560 (per year) or 280 (per semester) 
                                                </li>
                                            </ol>

                                        <h5><b>Job Title Skill Training – Skilling Certificate [ to be developed]   </b></h5>
                                           <p>
                                               Competency Based Training (CBT) aligns with Cambodia Common Competency Standard on Tourism Professional (CCCSTP) for Hotel Service and Travel Services.
                                           </p>
                                        <h5><b>School Fees  </b></h5>

                                        <br>
                                        <h3>Resources </h3>
                                        <ul>
                                            <li> - Three classrooms [description]  </li>
                                            <li> - A two storey application restaurant and café with a commercial kitchen [description] </li>
                                            <li> - A sample hotel room [description]</li>
                                            <li> - A laundry room [description]</li>
                                            <li> - A computer lab and a library [description]</li>
                                            <li> - A front desk [description]</li>
                                        </ul>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                        <!--End single item-->
                    </div>
                    <!--End top content-->


                    
                    
                    
                </div>
            </div>
            
            
            
        </div>
    </div>
</section>
<!--End Single service area-->





@endsection