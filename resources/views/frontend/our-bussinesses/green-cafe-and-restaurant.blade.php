@extends('frontend.layouts.master')

@section('title', 'Our Business | '.env('APP_NAME'))
@section('active-our-businesses', 'current')
@section('active-green-cafe', 'active')

@section ('content')
    
    @php($banner = getBanner(4))
    @if($banner)
        @include('frontend.layouts.banner', ['banner'=>['image'=>asset($banner->banner->image), 'title'=>'Green Cafe and Restaurant']])
    @endif 
  <!--Start Single service area-->
<section id="single-service-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-5 col-sm-7 col-xs-12 pull-left">
                @include('frontend.our-bussinesses.menu')
            </div> 
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 pull-right">
                <div class="content-box">
                    <!--Start top content-->
                    <div class="row top-content">
                        <!--Start single item-->
                        <div class="col-md-12">
                            <div class="single-item">
                                <div class="img-holder">
                                    <img src="{{ asset($data->banner) }}" alt="Awesome Image">
                                </div>
                                {!! $data->content !!}
                                <!-- <div class="text-holder">
                                    <h3>About Us</h3>
                                    <p class="mar-btm15">Green is an application Café and restaurant of the ATP School of Hospitality and Tourism. Green Café and Restaurant serves as a learning ambiance for ATP_School of Hospitality and Tourism where students can acquire real-to-life experience in food and beverage service and production. The Café and restaurant provides a wide range of products focusing on healthy diets to general customers.  Moreover, we also provide catering service for small functions like birthday party, staff party and anniversary. Our cafe and restaurant operates from 6.30 am to 8.30 pm daily.  Our business slogan is Green Café and Restaurant serves your favorite healthy food and drink.</p>
                                    
                                    <h3>Our Vision</h3>
                                    <p class="mar-btm15">
                                        The franchise casual fine dining café and restaurant in Cambodia under TM of “Green Café & Restaurant” to young investors throughout the country.
                                    </p>

                                    <h3>Mission</h3>
                                  
                                    <ol>
                                        <li>Builds capacity of young Cambodians to be able to manage and run the f&b business </li>
                                        <li>Provides healthy choices of food and drink to customers</li>
                                    </ol>

                                     <h3>Objective</h3>
                                        <div style="    padding: 5px 0px 0px 0px;" class="row">
                                            <div class="col-lg-4 col-md-4">
                                               <img src="{{ asset('public/frontend/images/our-bussiness/resautrant-obj.jpg') }}">
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                               <p class="mar-btm15">
                                                    <p>
                                                       <ul>
                                                           <li> - Trains young Cambodians the knowledge, skills and attitude in the hospitality industry</li>
                                                           <li> - Provides a first-hand working experience to Cambodian youth to manage the operation of a café and restaurant.</li>
                                                           <li> - Offers a wide range of healthy food and drink options in an affordable price. </li>
                                                           <li> - Offers MSG free food to customers</li>
                                                           <li> - Maximize the use of local produced and fresh products and food and beverage preparation. </li>
                                                       </ul>
                                                    </p>
                                                   <br />
                                                </p> 
                                            </div>
                                        </div>
                                        
                                </div> -->
                            </div>
                        </div>
                        <!--End single item-->
                    </div>
                    <!--End top content-->






                    
                    
                </div>
            </div>
            
             
            
        </div>
    </div>
</section>
<!--End Single service area-->





@endsection