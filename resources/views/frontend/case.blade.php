@extends('frontend.layouts.master')

@section('title', 'Our Business | '.env('APP_NAME'))
@section('active-our-businesses', 'current')


@section ('content')
    
   @include('frontend.layouts.banner', ['banner'=>['image'=>asset('public/frontend/CamCyber/banner-consultant.jpg'), 'title'=>'ASEAN Tourism Professional Consultant']])


   <div class="container">
        <div class="row" style="margin-top: 30px;">
            <div class="col-md-4">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset ('public/frontend/images/services/learn-1.jpg') }}">
                    <div class="card-body">
                    <!-- <h5 class="card-title">Card title</h5> -->
                    <p class="card-text">ATP Class</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset ('public/frontend/images/services/learn-2.jpg') }}" style="height: 133px;">
                    <div class="card-body">
                    <!-- <h5 class="card-title">Card title</h5> -->
                    <p class="card-text">ATP Class</p>
                    </div>
                </div>
            </div>
                
            <div class="col-md-4">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset ('public/frontend/images/services/learn-3.jpg') }}">
                    <div class="card-body">
                    <!-- <h5 class="card-title">Card title</h5> -->
                    <p class="card-text">ATP Class</p>
                    </div>
                </div>
            </div> 

        </div>

      

        <div class="row" style="margin-top: 30px;">
            <div class="col-md-4">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset ('public/frontend/images/services/learn-5.jpg') }}" style="height: 137px;">
                    <div class="card-body">
                    <!-- <h5 class="card-title">Card title</h5> -->
                    <p class="card-text">ATP Class</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset ('public/frontend/images/services/learn-6.jpg') }}">
                    <div class="card-body">
                    <!-- <h5 class="card-title">Card title</h5> -->
                    <p class="card-text">ATP Class</p>
                    </div>
                </div>
            </div>
                
            <div class="col-md-4">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset ('public/frontend/images/services/learn-7.jpg') }}">
                    <div class="card-body">
                    <!-- <h5 class="card-title">Card title</h5> -->
                    <p class="card-text">ATP Class</p>
                    </div>
                </div>
            </div> 


        </div>

        <div class="row" style="margin-top: 30px;">
            <div class="col-md-4">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset ('public/frontend/images/services/learn-9.jpg') }}" style="height: 137px;">
                    <div class="card-body">
                    <!-- <h5 class="card-title">Card title</h5> -->
                    <p class="card-text">ATP Class</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset ('public/frontend/images/services/learn-10.jpg') }}">
                    <div class="card-body">
                    <!-- <h5 class="card-title">Card title</h5> -->
                    <p class="card-text">ATP Class</p>
                    </div>
                </div>
            </div>
                
            <div class="col-md-4">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset ('public/frontend/images/services/learn-11.jpg') }}" style="height: 134px;">
                    <div class="card-body">
                    <!-- <h5 class="card-title">Card title</h5> -->
                    <p class="card-text">ATP Class</p>
                    </div>
                </div>
            </div> 


        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset ('public/frontend/images/services/learn-4.jpg') }}">
                    <div class="card-body">
                    <!-- <h5 class="card-title">Card title</h5> -->
                    <p class="card-text">ATP Class</p>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset ('public/frontend/images/services/learn-8.jpg') }}">
                    <div class="card-body">
                    <!-- <h5 class="card-title">Card title</h5> -->
                    <p class="card-text">ATP Class</p>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset ('public/frontend/images/services/learn-12.jpg') }}">
                    <div class="card-body">
                    <!-- <h5 class="card-title">Card title</h5> -->
                    <p class="card-text">ATP Class</p>
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection