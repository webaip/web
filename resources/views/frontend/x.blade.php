<html>
    <head>
        <title>Getting data from DB</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>

    <body>
        <div class="container">
            <h1>Products</h1>
            @if(count($data) > 0)
            <table class="table table-striped">
                <thead>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Price (USD)</th>
                </thead>
                <tbody>
                    @php( $total = 0 )
                    @foreach($data as $row)
                        <tr id="row-{{ $row->id }}" >
                            <td>{{ $row->id }}</td>
                            <td>{{ $row->name }}</td>
                            <td><img src="{{ asset($row->image) }}" class="img img-responsive" /></td>
                            <td>{{ $row->price }}</td>
                            <td>
                                <button onClick="add('{{ $row->name }}')" class="btn btn-success"><span class="glyphicon glyphicon-shopping-cart"></span></button>
                                <button onClick="remove({{ $row->id }})" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></button>
                            </td>
                        </tr>
                        @php( $total += $row->price )
                    @endforeach
                    <tr>
                        <td></td>
                         <td></td>
                        <td></td>
                        <td></td>
                        <td><b>{{ $total }}USD</b></td>
                    </tr>
                </tbody>
            </table>
            <hr />
            @else
                <span>No Data</span>
            @endif
        </div>

        <script >
            function add(name){
                alert('Your product '+ name +' has been added.');
            }
            function remove(id){
                document.getElementById("row-"+id).setAttribute("style", "display:none");
            }
        </script>
    </body>
</html>


        