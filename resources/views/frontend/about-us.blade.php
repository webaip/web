@extends('frontend.layouts.master')

@section('title', 'About Us | '.env('APP_NAME'))
@section('active-about-us', 'current')


@section ('content')
    @php($banner = getBanner(1))
    @if($banner)
        @include('frontend.layouts.banner', ['banner'=>['image'=>asset($banner->banner->image), 'title'=>'About Us']])
    @endif


    <section class="about-top-area">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="img-holder">
                        <img src="{{ asset($about_us->image) }}" alt="Awesome Image">    
                    </div>    
                </div>
                <div class="col-md-4">
                    <div class="text-holder">
                        <h3>{{$about_us->title}}</h3>
                        <p>{!!$about_us->description!!}</p>
                        <div class="bottom">
                            <h3>{{$about_us->ceo_name}}</h3>
                            <span>{{$about_us->ceo_position}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <ul class="company-details">
                        <li>
                            <div class="icon-holder">
                                <span class="flaticon-people"></span>
                            </div>
                            <div class="text-box">
                                <h3>Our Mission</h3>
                                <p>{!!$about_us->mission!!}</p>
                            </div>
                        </li>
                        <li>
                            <div class="icon-holder">
                                <span class="flaticon-arrow"></span>
                            </div>
                            <div class="text-box">
                                <h3>Our Vision</h3>
                                <p>{!!$about_us->vision!!}</p>
                            </div>
                        </li>
                        <li>
                            <div class="icon-holder">
                                <span class="flaticon-luxury"></span>
                            </div>
                            <div class="text-box">
                                <h3>Our Value</h3>
                                <p>{!!$about_us->value!!}</p>
                            </div>
                        </li>
                        
                    </ul>
                </div>
                
            </div>
        </div>
    </section>
    <!--End About Top Area-->

    <div class="tp-parallax-wrap visible-lg">
        <div class="tp-parallax-wrap sreen-responsive" style="position: absolute;top: 227px; z-index: 17;left:140px;">
            <div class="tp-loop-wrap">
                <div class="tp-mask-wrap">
                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme tp-static-layer" id="slider-1-layer-26" data-x="['right','right','center','left']" data-hoffset="['20','20','-4','50']" data-y="['middle','top','top','top']" data-voffset="['19','195','513','300']" data-width="['351','343','393','378']" data-height="['451','437','439','407']" data-whitespace="nowrap" data-type="shape" data-responsive_offset="on" data-startslide="0" data-endslide="2" data-frames="[{&quot;delay&quot;:0,&quot;speed&quot;:300,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]" data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 17; font-family: &quot;Open Sans&quot;; visibility: inherit; transition: none 0s ease 0s; text-align: inherit; line-height: 24px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 400; font-size: 15px; white-space: nowrap; min-height: 451px; min-width: 351px; max-height: 451px; max-width: 351px; opacity: 1; transform-origin: 50% 50% 0px; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);left:300;"> 
                       <div class="slide-menu">
                           <h3>Our Business</h3>
                           <div class="biz-cnt">
                               <a href= "{{ route('our-business', ['biz'=>'atp-consultant']) }}">
                                    <div class="item">
                                       ATP Consultant Co., Ltd
                                   </div>
                                <a>
                                
                                <a href= "{{ route('our-business', ['biz'=>'atp-school-of-hospitality-and-tourism']) }}">
                               <div class="item">
                                   ATP School of Hospitality and Tourism
                               </div>
                               <a>

                               <a href= "{{ route('our-business', ['biz'=>'green-cafe-and-restaurant']) }}">
                               <div class="item">
                                 Green Café and Restaurant
                               </div>
                               <a>
                           </div>
                       </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection