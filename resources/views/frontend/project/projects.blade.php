@extends('frontend.layouts.master')

@section('title', 'Projects | '.env('APP_NAME'))
@section('active-projects', 'current')
@section(last(request()->segments()), 'active')

@section ('content')
    @php($banner = getBanner(7))
    @if($banner)
        @include('frontend.layouts.banner', ['banner'=>['image'=>asset($banner->banner->image), 'title'=>'Project']])
    @endif 
    <section class="services-page-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-5 col-sm-7 col-xs-12 pull-left">
                
                @include('frontend.project.menu')
                
            </div>  
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 pull-right">
                @if($data)
                <div class="service-content">
                @php($route = last(request()->segments()))
                  
                    @foreach($data->chunk(3) as $item)
                    <div class="row">
                        <!--Start single item-->
                        @foreach($item as $row)
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-service-item text-center">
                                <div class="img-holder">
                                    <img src="{{ asset($row->image) }}" alt="Awesome Image"> 
                                    <div class="inner-content">
                                        <h3><a class="title" href="{{ route('project-detail',['biz'=>$route,'slug'=>$row->slug]) }}">{{$row->title}}</a></h3>
                                        <div class="text">
                                            <p>{{str_limit($row->description,90)}}</p>
                                        </div>
                                    </div>
                                    <div class="overlay">
                                        <div class="box">
                                            <div class="content">
                                                <div class="overlay-content">
                                                    <div class="icon-holder">
                                                        
                                                    </div>
                                                    <h3><a class="title" href="{{ route('project-detail',['biz'=>$route,'slug'=>$row->slug]) }}">{{$row->title}}</a></h3>
                                                    <div class="text">
                                                        <p>{{str_limit($row->description,90)}}</p>
                                                    </div>
                                                    <div class="read-more">
                                                        <a href="{{ route('project-detail',['biz'=>$route,'slug'=>$row->slug]) }}">Read More</a>
                                                    </div>    
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                </div>    
                            </div>    
                        </div>
                        @endforeach
                        <!--End single item-->
                        
                    </div>
                    @endforeach
                </div>  
                <div class="row">
                    <div class="col-md-12">
                        {{ $data->links('vendor.pagination.frontend-html') }}
                    </div>
                </div>  
                @else
                    <p>No Data</p>
                @endif
            </div>
                
        </div>
    </div>
</section>


<!--Start Google map area-->
<section class="contact-area">
            <iframe
              width="100%"
              height="450"
              frameborder="0" style="border:0"
              src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCOVmFlwfcjVJE1mgzI69HIOnIwLYEW1OM
                &q=%ATP+School+of+Hospitality+and+Tourism" allowfullscreen>
            </iframe>
</section>
<!--End Google map area--> 


@endsection