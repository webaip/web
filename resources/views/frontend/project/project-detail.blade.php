@extends('frontend.layouts.master')

@section('title', 'Projects | '.env('APP_NAME'))
@section('active-projects', 'current')
@section(request()->segments()['1'], 'active')

@section ('content')
    
@php($banner = getBanner(8))
    @if($banner)
        @include('frontend.layouts.banner', ['banner'=>['image'=>asset($banner->banner->image), 'title'=>'Project Detail']])
    @endif 
    <section class="services-page-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-5 col-sm-7 col-xs-12 pull-left">
                
                @include('frontend.project.menu')
                
            </div>  
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 pull-right">
                <div class="service-content">
                    <div class="row">
                       
                    <!--Start top content-->
                    <div class="row top-content">
                        <!--Start single item-->
                        <div class="col-md-12">
                            <div class="single-item">
                                <div class="img-holder">
                                @if($data) <img src="{{ asset($data->image_detail) }} "  alt="Awesome Image">@endif
                                </div>
                                <div class="text-holder">
                                   <p>@if($data) {!!$data->content!!} @endif</p>
                                   
                                </div>
                            </div>
                        </div>
                        <!--End single item-->
                    </div>
                    <!--End top content-->
                        
                    </div>
                </div>    
            </div>
                
        </div>
    </div>
</section>





@endsection