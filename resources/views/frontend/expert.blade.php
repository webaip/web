@extends('frontend.layouts.master')

@section('title', 'About Us | '.env('APP_NAME'))
@section('active-about-us', 'current')


@section ('content')
    @php($banner = getBanner(1))
    @if($banner)
        @include('frontend.layouts.banner', ['banner'=>['image'=>asset($banner->banner->image), 'title'=>'About Us']])
    @endif


<div class="container">
        <div class="row">
            <div class="col-md-9" style="background: #8080800a; margin-top: 51px;margin-bottom: 60px;">
                <div class="title" style="text-align: center; color: #1a63ae;">
                    <h2 style="margin-top: 21px;">Our Competitive Strengths</h2>
                </div>

                <div class="compitives" style="margin-top: 30px;">

                                <div class="one_half">
                                    <h2 style="color: #1a63ae;">Left Side Strenghts</h2>
                                    <div class="list-style" style="padding-left: 20px; margin-top: 10px;">
                                        <ul style="color:black;">
                                            <li>Detail- and fact-oriented</li>
                                            <li>Critical thinking</li>
                                            <li>Analytical</li>
                                        </ul>
                                    </div>                    
                                </div>
                                
                                <div class="menu-pull-right" style="float: right; margin-top: -120px; padding-bottom: 100px;">
                                    <h2 style="color: #1a63ae;">Right Side Strenghts</h2>
                                    <div class="list-style" style="padding-left: 20px; margin: 10px;">
                                        <ul style="color: black;">
                                            <li>Intuitive</li>
                                            <li>Crestive</li>
                                            <li>Visualize more than in words</li>
                                        </ul>
                                    </div>
                                </div>
                </div>

                   
                <div class="img" style="margin-top: 50px;">
                    <div class="col-md-6">
                        <img src="{{ asset ('public/frontend/images/team/alots.jpg') }}" style="border-style: solid;">
                            <p style="color:black;">We generate innovative concepts for experiences and then test and verify their strength.</p>
                            
                    </div>
                        <div class="col-md-6">
                            <p style="margin-top: -57px;color:black;">In applying these competitive strengths, we are renown for developing innovative tourism experiences
                            that reflect specific target market needs and strengthen the competitive advantage of a destination or
                            business.</p>
                            <p style="margin-top: 52px;color:black;">We
                            have concentrated this specialisation through ecotourism, cultural tourism and food and wine sectors.
                            We are not ex bureaucrats – we have been directly responsible for funding, developing and operating
                            tourism businesses.</p>
                        </div>
                </div>

            </div>

            <div class="col-md-3">
                <div class="title" style="color: #1e72bd; background: #8080800a; height: 620px; width: 317px;    margin-top: 51px;">
                    <h2 style="text-align: center; margin-top: 1px; padding: 10px;color: #1e72bd;">Services</h2>
                    <div class="link" style="color: #1e72bd;">
                        <li style="padding: 5px;margin-top: 18px; padding-left: 17px;"><a href="{{ route('services') }}">Job Matching & Coaching</a></li>
                        <li style="padding: 5px;margin-top: 18px; padding-left: 17px;"><a href="{{ route('services') }}">Training & Customized</a></li>  
                        <li style="padding: 5px;margin-top: 18px; padding-left: 17px;"><a href="{{ route('services') }}">Rebooting Business Training</a></li>
                        <li style="padding: 5px;margin-top: 18px; padding-left: 17px;"><a href="{{ route('services') }}">& MSME Bussiness startups</a></li>
                    </div>   
                    <div class="text">
                        <h2 style="text-align: center; margin-top: 22px; margin-right: 10px;color: #1e72bd;">Contact Us</h2>                    
                        <p style="margin-top: 16px; padding-left: 17px;color:black;">Email: info@atpconsultant.com</p>
                        <p style="padding-left: 17px;color:black;">Mobile: 010 696 099 | 012 398 353</p>
                        <p style="padding-left: 17px;color:black;">Office 1:</p>
                        <p style="padding-left: 17px;color:black;">Street 150, H#166, Sangkat Teuk LaakII, Khan Toul Kork, Phnom Penh of Cambodia</p>
                    </div>
                    <div class="icon" style="text-align: center;">
                        <i class="fa fa-phone" aria-hidden="true" style="height: 30px; width: 30px;"></i>
                        <i class="fa fa-linkedin" aria-hidden="true" style="height: 30px; width: 30px;"></i>
                        <a href="https://www.facebook.com/atpconsultant"><i class="fa fa-facebook-official" aria-hidden="true" style="height: 30px; width: 30px;"></i></a>
                        
                    </div>   
                </div>
            </div>

        </div>

    </div>

    @endsection    