@extends('frontend.layouts.master')

@section('title', 'Our Business | '.env('APP_NAME'))
@section('active-our-businesses', 'current')


@section ('content')
    
   @include('frontend.layouts.banner', ['banner'=>['image'=>asset('public/frontend/CamCyber/banner-consultant.jpg'), 'title'=>'ASEAN Tourism Professional Consultant']])
    <!--Start Single service area-->
<section id="single-service-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 pull-right">
                <div class="content-box">
                    <!--Start top content-->
                    <div class="row top-content">
                        <!--Start single item-->
                        <div class="col-md-12">
                            <div class="single-item">
                                <!-- <div class="img-holder">
                                    <img src="{{ asset('public/frontend/CamCyber/consultant.jpg') }}" alt="Awesome Image">
                                </div> -->
                                <div class="text-holder">
                                    <h3>About Us</h3>
                                    <p class="mar-btm15">ATP Consultant Co., LTD was established in 2015 by Cambodian Hotel and Travel Service Expert. ATP Expert team were recognized as ASEAN Master Trainer and Master Assessor and operating the consulting service under legal requirement and certified by Ministry of tourism of Cambodia, National committee for tourism professionals.</p>
                                    
                                    <h3>Our Vision</h3>
                                    <p class="mar-btm15">
                                        OUR VISION is to be a leader in ASEAN Skills Training, Assessment and Consulting in Hospitality and Tourism Industry.
                                    </p>

                                    <h3>Mission</h3>
                                    <p class="mar-btm15">
                                        OUR MISSION is to Develop Human Resources in Hotel and Travel Services in Hospitality and Tourism Industries based on ASEAN Mutual Recognition Arrangement for Tourism Professionals Qualification Reference Framework by providing Skills, Knowledge and right Attitude to our partners. And To provide professionals consulting with ethic and quality services base on National Qualification and ASEAN Mutual Recognition Arrangement for Tourism Professionals Qualification Framework.

                                    </p>

                                     <h3>Objective</h3>
                                    <p class="mar-btm15">
                                       <h5>For Nation</h5>
                                       <p>
                                           <ul>
                                               <li> - To Build Human Capital on Hospitality and Tourism Professionals in Cambodia</li>
                                               <li> - To serve Ministry of Tourism of Cambodia on Human Resource Development Strategy.</li>
                                               <li> - To Increase Hospitality and Tourism Professionals in Cambodia. </li>
                                           </ul>
                                       </p>

                                       <br />
                                       <h5>For Partners</h5>
                                       <p>
                                           <ul>
                                                <li> - To get recognized qualification of Training and Assessment by NCTP, Ministry of Tourism of Cambodia. </li>
                                                <li> - To get skills passport recording in Hospitality and Tourism Skills Passport. </li>
                                                <li> - To get registering to ASEAN Tourism Professionals in Registration System. </li>
                                                <li> - To get opportunity of Tourism Professionals Mobility in local and international.  </li>
                                                <li> - To get trust on quality and ethics services. </li>
                                           </ul>
                                       </p>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--End single item-->
                    </div>
                    <!--End top content-->


                    <!--Start Specific Services-->
                    <div class="specific-services">
                        <div class="sec-title">
                            <h3>Specific Services</h3>
                            <span class="border"></span>
                        </div>
                        <div class="row">
                            <!--Start single item-->
                            <div class="col-lg-4 col-md-4">
                                <div class="single-item">
                                    <div class="icon-holder">
                                        <span class="flaticon-business-1"></span>    
                                    </div>
                                    <div class="text-holder">
                                        <h3>Consultant</h3>
                                        <p>Which of ever undertke laborious physical exercised excepts.</p>
                                       
                                    </div>    
                                </div>  
                            </div>
                            <!--End single item-->
                            <!--Start single item-->
                            <div class="col-lg-4 col-md-4">
                                <div class="single-item">
                                    <div class="icon-holder">
                                        <span class="flaticon-graphic"></span>    
                                    </div>
                                    <div class="text-holder">
                                        <h3>Training</h3>
                                        <p>Great explorer the master builder working human happiness.</p>
                                        
                                    </div>    
                                </div>  
                            </div>
                            <!--End single item-->
                            <!--Start single item-->
                            <div class="col-lg-4 col-md-4">
                                <div class="single-item">
                                    <div class="icon-holder">
                                        <span class="flaticon-technology-1"></span>    
                                    </div>
                                    <div class="text-holder">
                                        <h3>Assessment</h3>
                                        <p>Chooses to enjoy a pleasure that has no anoing consequences.</p>
                                        
                                    </div>    
                                </div>  
                            </div>
                            <!--End single item-->
                        </div>
                    </div>
                    <!--End Specific Services-->




                    <!--Start Benefits Service content-->
                    <div class="benefits-service-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="sec-title">
                                    <h3>Key Benefits of the Service</h3>
                                    <span class="border"></span>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-12">
                                        <div class="text-holder">
                                            <p>Beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue beguiled and demoralized.</p> 
                                            <ul>
                                                <li><i class="fa fa-angle-right" aria-hidden="true"></i>Develop new ideas and market them</li>
                                                <li><i class="fa fa-angle-right" aria-hidden="true"></i>Build leadership and management skills</li>
                                                <li><i class="fa fa-angle-right" aria-hidden="true"></i>Improve manufacturing processes</li>
                                                <li><i class="fa fa-angle-right" aria-hidden="true"></i>Build a business strategy and plan</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12">
                                        <div class="video-holder">
                                            <img src="{{ asset('public/frontend/images/services/service-single/video-gallery.jpg') }}" alt="Awesome Video Gallery">
                                            <div class="overlay-gallery">
                                                <div class="icon-holder">
                                                    <div class="icon">
                                                        <a class="html5lightbox" title="Consultive Video Gallery" href="https://www.youtube.com/watch?v=-VW_tLcGgwA"><img src="{{ asset('public/frontend/images/icon/play-btn.png') }}" alt="Play Button"></a>   
                                                    </div>
                                                </div>    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End Benefits Service content-->
                    <!--Start accordion box-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="accordion-box">
                                <!--Start single accordion box-->
                                <div class="accordion accordion-block">
                                    <div class="accord-btn">
                                        <h4>What is the procedure to join with your company?</h4>
                                    </div>
                                    <div class="accord-content">
                                        <p>The master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure. </p>
                                    </div>
                                </div>
                                <!--End single accordion box--> 
                                <!--Start single accordion box-->
                                <div class="accordion accordion-block">
                                    <div class="accord-btn active">
                                        <h4>Do you give any offer for premium customer?</h4>
                                    </div>
                                    <div class="accord-content collapsed">
                                        <p>The master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure. </p>
                                    </div>
                                </div>
                                <!--End single accordion box-->
                                <!--Start single accordion box-->
                                <div class="accordion accordion-block last">
                                    <div class="accord-btn last">
                                        <h4>What makes you special from others?</h4>
                                    </div>
                                    <div class="accord-content last">
                                        <p>The master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure. </p>
                                    </div>
                                </div>
                                <!--End single accordion box-->       
                            </div>
                        </div>
                    </div>
                    <!--End accordion box-->
                    
                </div>
            </div>
            
            <div class="col-lg-3 col-md-5 col-sm-7 col-xs-12 pull-left">
                <div class="left-sidebar">
                    <!--Start single sidebar-->
                    <div class="single-sidebar">
                        <ul class="page-link">
                            <li><a class="active" href="{{ route('our-business', ['biz'=>'asean-tourism-professional-consultant']) }}">Asean Tourism Professional Consultant</a></li>
                            <li><a href="{{ route('our-business', ['biz'=>'school-of-hospitality-and-tourism']) }}">School of Hospitality and Tourism</a></li>
                            <li><a href="{{ route('our-business', ['biz'=>'green-cafe-and-restaurant']) }}">Green Cafe and Restaurant</a></li>   
                        </ul>
                    </div>
                    <!--End single sidebar-->
                    
                  
                    
                    <!--Start single sidebar-->
                    <div class="single-sidebar">
                        <ul class="brochures-dwn-link">
                            <li>
                                <a href="#">
                                    <div class="icon-holder">
                                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i>  
                                    </div>
                                    <div class="title-holder">
                                        <h5>Download Company Profile</h5>    
                                    </div>
                                </a>
                            </li>
                           
                        </ul> 
                    </div>
                    <!--End single sidebar-->
                    
                </div>
            </div>  
            
        </div>
    </div>
</section>
<!--End Single service area-->





@endsection