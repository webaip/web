@extends('frontend.layouts.master')

@section('title', 'Welcome to ATP')
@section('active-home', 'current')

@section ('appbottomjs')
<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection
@section ('content')

    <!--Start rev slider wrapper-->     
    <section class="rev_slider_wrapper">
        <div id="slider1" class="rev_slider"  data-version="5.0">
            <ul>
                @foreach($slides as $row)
                <li data-transition="fade">
                    <img src="{{ asset($row->image) }}"  alt="" width="1900" height="700" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1">
                    
                    <div class="tp-caption  tp-resizeme" 
                        data-x="left" data-hoffset="0" 
                        data-y="top" data-voffset="203" 
                        data-transform_idle="o:1;"         
                        data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                        data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                        data-mask_in="x:[100%];y:0;s:inherit;e:inherit;" 
                        data-splitin="none" 
                        data-splitout="none"
                        data-responsive_offset="on"
                        data-start="700">
                        <div class="slide-content-box ">
                            <div class="big-title" style="color:{{$row->color}};">{!!$row->title!!}</div>
                        </div>
                    </div>
                </li>
                @endforeach



            </ul>
        </div>
       
    </section>
    <div class="tp-parallax-wrap visible-lg">
        <div class="tp-parallax-wrap sreen-responsive" style="position: absolute;top: 227px; z-index: 17;left:140px;">
            <div class="tp-loop-wrap">
                <div class="tp-mask-wrap">
                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme tp-static-layer" id="slider-1-layer-26" data-x="['right','right','center','left']" data-hoffset="['20','20','-4','50']" data-y="['middle','top','top','top']" data-voffset="['19','195','513','300']" data-width="['351','343','393','378']" data-height="['451','437','439','407']" data-whitespace="nowrap" data-type="shape" data-responsive_offset="on" data-startslide="0" data-endslide="2" data-frames="[{&quot;delay&quot;:0,&quot;speed&quot;:300,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]" data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 17; font-family: &quot;Open Sans&quot;; background-color: rgba(132, 132, 132, 0.5); visibility: inherit; transition: none 0s ease 0s; text-align: inherit; line-height: 24px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 400; font-size: 15px; white-space: nowrap; min-height: 451px; min-width: 351px; max-height: 451px; max-width: 351px; opacity: 1; transform-origin: 50% 50% 0px; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);left:300;"> 
                       <div class="slide-menu">
                           <h3>Our Business</h3>
                           <div class="biz-cnt">
                               <a href= "{{ route('our-business', ['biz'=>'atp-consultant']) }}">
                                    <div class="item">
                                       ATP Consultant Co., Ltd
                                   </div>
                                <a>
                                
                                <a href= "{{ route('our-business', ['biz'=>'atp-school-of-hospitality-and-tourism']) }}">
                               <div class="item">
                                   ATP School of Hospitality and Tourism
                               </div>
                               <a>

                               <a href= "{{ route('our-business', ['biz'=>'green-cafe-and-restaurant']) }}">
                               <div class="item">
                                 Green Café and Restaurant
                               </div>
                               <a>
                           </div>
                       </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End rev slider wrapper--> 

    <!--Start choose area-->  
    <section class="choose-area sec-pding-two">
        <div class="container">
            <div class="sec-title text-center">
                <h1>S</h1>
                <div class="overlay-title">
                    <h2>Why Choose Us?</h2> 
                </div>   
            </div>
            <div class="row">
                <!--Start single choose item-->
                <div class="col-md-6">
                    <div class="single-choose-item">
                        <div class="top clearfix">
                            <div class="icon-holder">
                                <img src="{!! getContentImage('professional-reliably-content') !!}" />
                            </div>
                            <div class="title-holder">
                                <h3>Professional & Reliably</h3>
                            </div>
                        </div>
                        <div class="text-holder">
                            <p>{!! getContent('professional-reliably-content') !!}</p>
                        </div>
                      
                    </div>
                </div>
                <!--End single choose item-->
                <!--Start single choose item-->
                <div class="col-md-6">
                    <div class="single-choose-item">
                        <div class="top clearfix">
                            <div class="icon-holder">
                                <img src="{!! getContentImage('experience-content') !!}" />
                            </div>
                            <div class="title-holder">
                                <h3>Experience </h3>
                            </div>
                        </div>
                        <div class="text-holder">
                            <p>{!! getContent('experience-content') !!}</p>
                        </div>
                       
                    </div>
                </div>
                <!--End single choose item-->
            </div>
        </div>
    </section>  
    <!--End choose area-->    

    <!--Start services area-->
    <section class="services-area sec-pding-two">
        <div class="container">
            <div class="sec-title">
                <h1>P</h1>
                <div class="overlay-title">
                    <h2>Our Projects</h2> 
                </div>   
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="service-carousel">
                        @foreach($projects as $row)
                        @if($row->service_id == 1)
                            @php($route = 'atp-consultant')
                        @elseif($row->service_id == 2)
                            @php($route = 'atp-school-of-hospitality-and-tourism')
                        @else
                        @php($route = 'green-cafe-and-restaurant')
                        @endif
                        <!--Start single item-->
                        <div class="single-service-item text-center">
                            <div class="img-holder">
                                <img src="{{ asset($row->image) }}" alt="Awesome Image"> 
                                <div class="inner-content">
                                    <h3><a class="title" href="{{ route('project-detail',['biz'=>$route,'slug'=>$row->slug]) }}">{{$row->title}}</a></h3>
                                    <div class="text">
                                        <p>{{str_limit($row->description,90)}}</p>
                                    </div>
                                </div>
                                <div class="overlay">
                                    <div class="box">
                                        <div class="content">
                                            <div class="overlay-content">
                                                <div class="icon-holder">
                                                    
                                                </div>
                                                <h3><a class="title" href="{{ route('project-detail',['biz'=>$route,'slug'=>$row->slug]) }}">{{$row->title}}</a></h3>
                                                <div class="text">
                                                    <p>{{str_limit($row->description,90)}}</p>
                                                </div>
                                                <div class="read-more">
                                                    <a href="{{ route('project-detail',['biz'=>$route,'slug'=>$row->slug]) }}">Read More</a>
                                                </div>    
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </div>    
                        </div>    
                        <!--End single item-->
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End services area-->

    <section class="services-area sec-pding-two">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{ asset ('public/frontend/images/blog/lat-blog-8.jpg')}}" style="width: 400px; height: auto; float: right;" alt="Awesome Image">
                </div>
                <div class="col-md-6">
                    <div class="sec-title">
                        <h1 style="text-align: center;">M</h1>
                        <div class="overlay-title" style="text-align: center;">
                            <h2 style="color: #1a63ae">Our Missions</h2>
                        </div>
                        <p>We are provide intimately professional consulting with ethic and
                           quality of services                  for our Hotel and Travel service partners.</p>
                    </div>
                </div>  
            </div>
        </div>
    </section>
        
        <!--Start fact counter area-->
    <section class="fact-counter-area" style="background-image:url({{ asset('public/frontend/images/resources/fact-counter-bg.jpg') }});">
        <div class="container">
            <div class="title text-center">
                <h3>{!! getContent('solutions') !!}</h3>
            </div>
            <div class="row">
                @foreach($informations as $row)
                <!--Start single item-->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="single-item text-center">
                        <h1><span class="timer" data-from="1" data-to="{{$row->number}}" data-speed="5000" data-refresh-interval="{{$row->number}}">{{$row->number}}</span></h1>
                        <h3>{{$row->name}}</h3>
                    </div>    
                </div>
                <!--End single item-->
                @endforeach

         
            </div>
        </div>
    </section>
    <!--End fact counter area-->

    <!--Start project area-->
    <section class="latest-project-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="sec-title pull-left">
                        <h1>p</h1>
                        <div class="overlay-title">
                            <h2>Recent Activities</h2> 
                        </div>   
                    </div>
                    
                </div>
            </div>
            <div class="row inner-content">
                <div class="col-md-7 left-content">
                    <div class="projects">
                        @foreach($activities as $row)
                        <!--Start single project item-->
                        <div class="single-project-item">
                            <div class="img-holder">
                                <img src="{{ asset($row->image) }}" alt="Awesome Image">
                                <div class="overlay-style-one">
                                    <div class="box">
                                        <div class="content">
                                            <ul>
                                                <li>
                                                    <a class="zoom" href="{{ asset($row->image) }}" data-rel="prettyPhoto" title="{{$row->title}}">
                                                        <i class="fa fa-search" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <!-- <li>
                                                    <a class="link" href="project-single.html"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                                </li> -->
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>   
                        </div> 
                        <!--End single project item-->
                        @endforeach
                    </div>    
                </div>
                <div class="col-md-5 right-content">
                    <div class="text-holder">
                        <div class="content">
                            <!-- <h3>Consumer Products</h3>
                            <p>{!! getContent('consumer-products') !!}</p> -->
                           <div class="row">
                         <div class="col-md-1 ">
                        
                       </div> 
                       <div class="col-md-11 ">
                        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2">
                
                 </script>
                    <div class="page-header text-center">
                           <h1 class="text-center font-i">ព័ត៌មានបណ្តាញសង្គម</h1>
                    </div>
                    <div class="fb-pages">

                        <div class="fb-page" data-href="https://www.facebook.com/ATP.School.Hospitality.Tourism.Cambodia/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                        <blockquote cite="https://www.facebook.com/Sad-225921294665768/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Sad-225921294665768/">ATP School of Hospitality and Tourism</a></blockquote>
                     </div> 
                       </div>  
                      </div> 
                     </div>
                         
                        </div>
                    </div>  
                     
                </div>
                
            </div>
        </div>
    </section>  
    <!--End project area--> 

    <!--Start latest blog area-->
    <section class="latest-blog-area">
        <div class="container">
            <div class="sec-title text-center">
                <h1>n</h1>
                <div class="overlay-title">
                    <h2>Our Latest News</h2> 
                </div>   
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="blog-carousel">
                        
                        <!--Start single latest blog-->
                        @foreach($news as $row)
                        @if($row->service_id == 1)
                            @php($route = 'atp-consultant')
                        @elseif($row->service_id == 2)
                            @php($route = 'atp-school-of-hospitality-and-tourism')
                        @else
                        @php($route = 'green-cafe-and-restaurant')
                        @endif
                        <div class="single-blog-post">
                            <div class="img-holder">
                                <img src="{{ asset($row->image) }}" alt="Awesome Image">
                                <div class="overlay-style-one">
                                    <div class="box">
                                        <div class="content">
                                            <a href="{{ route('news-detail',['biz'=>$route,'slug'=>$row->slug]) }}"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-holder">
                                <ul class="meta-info">
                                    <li><a href="{{ route('news-detail',['biz'=>$route,'slug'=>$row->slug]) }}">{{ Carbon\Carbon::parse($row->created_at)->format('d') }}-{{ Carbon\Carbon::parse($row->created_at)->format('F') }}-{{ Carbon\Carbon::parse($row->created_at)->format('Y') }}</a></li>
                                </ul>
                                <a href="{{ route('news-detail',['biz'=>$route,'slug'=>$row->slug]) }}">
                                    <h3 class="blog-title">{{$row->title}}</h3>
                                </a> 
                                <div class="text">
                                    <p>{{str_limit($row->description,90)}}</p>
                                </div>
                                <div class="bottom clearfix">
                                    <div class="readmore pull-left">
                                           
                                    </div>
                                    <div class="comment pull-right">
                                        <a href="{{ route('news-detail',['biz'=>$route,'slug'=>$row->slug]) }}">Read More</a> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <!--End single latest blog-->
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End latest blog area--> 

    <!--Start consultation area-->

    <section id="submit-consultant" class="consultation-area" style="background-image:url({{ asset('public/frontend/images/parallax-background/consultaion-bg.jpg') }});">
        <div class="container">
            <div class="row">
                
                <div class="col-md-8 col-sm-12 col-xs-12">
                    
                    <div class="consultation">
                        <div class="sec-title">
                            <h1>C</h1>
                            <div class="overlay-title">
                                <h2>Get a Free Consultation</h2> 
                            </div>   
                        </div>
                        @if(Session::has('msg'))
                        <div class="alert alert-success" role="alert">
                            Your request has been sent! We will respone you soon.
                        </div>
                        @endif
                        @if (count($errors) > 0)
                        <div class="alert alert-danger" role="alert">
                                Sending fail! Please Try again!
                            </div>
                        @endif
                        

                        @if (count($errors) > 0)
                            
                            @foreach ($errors->all() as $error)
                            <div class="alert alert-danger" role="alert">
                                {{ $error }}
                            </div>
                            @endforeach
                        @endif
                        <form id="contact_form" action="{{ route('submit-consultant') }}" method="post" class="consultation-form">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                            <div class="row">
                                <div class="col-md-6">   
                                    <input type="text" name="name" value="" placeholder="Your Name" required="">
                                    <input type="email" name="email" value="" placeholder="Your E-Mail" required="">
                                    <select class="selectmenu" name="services">
                                        <option selected="selected">Select Service</option>
                                        <option>Tourism</option>
                                        <option>Traning</option>
                                        <option>Green Cofe</option>
                            </select>
                                </div>
                                <div class="col-md-6">
                                    <textarea name="message" placeholder="Special Request (eg: date, time, expert)" required=""></textarea>   
                                    <button class="thm-btn" type="submit">Submit Here</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="experience-box text-center">
                        <h3>{!! $consultant_content->name !!}</h3>
                        <div class="year">
                            <h1>{!! $consultant_content->num_of_experince !!}</h1>
                            <h2>{!! $consultant_content->experince !!}</h2>
                        </div>
                        <p>{!! $consultant_content->description !!}</p>
                    </div>
                </div>
                
            </div>
        </div>
    </section>

    <!--End consultation area-->

    <!--Feedback Section-->
    <section class="feedback-section">
        <div class="container">
            <div class="sec-title text-center">
                <h1>F</h1>
                <div class="overlay-title">
                    <h2>Customer Feedback</h2> 
                </div>   
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="carousel-outer">
                        <div class="single-item-carousel owl-carousel owl-theme">

                            <!--Testimonial Block Three-->
                            @foreach($feedbacks as $row)
                            <div class="testimonial-block-three">
                                <div class="inner-box">
                                    <div class="upper-box">
                                        <div class="content">
                                            <div class="text">{{$row->description}}</div>
                                        </div>
                                        <div class="quote-icon"><span class="icon flaticon-double-quotes"></span></div>
                                    </div>
                                    <div class="lower-box">
                                        <div class="image-box">
                                            <img src="{{ asset($row->image) }}" alt=""/>
                                        </div>
                                        <h3>{{$row->name}}</h3>
                                        <div class="location">{{$row->address}}</div>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Feedback Section-->

    <!--Start Brand area-->  
    <section class="brand-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="brand">
                        <!--Start single item-->
                        @foreach($partners as $row)
                            <div class="single-item">
                                <a target="_blank" href="{{$row->url}}">
                                    <img src="{{ asset($row->image) }}" alt="{{$row->title}}">
                                </a>
                            </div>
                        @endforeach
                        <!--End single item-->
                        
                       
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Brand area--> 

@endsection