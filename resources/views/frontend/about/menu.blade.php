<div class="left-sidebar">
    <!--Start single sidebar-->
    <div class="single-sidebar">
        <ul class="page-link">
            <li><a class="@yield('team')"  href="{{ route('team', ['biz'=>'team']) }}">Advisor Team</a></li>
            <li><a class="@yield('expert')"  href="{{ route('expert', ['biz'=>'expert']) }}">Expert Team</a></li>
            <!-- <li><a class="@yield('atp-school-of-hospitality-and-tourism')" href="{{ route('projects', ['biz'=>'atp-school-of-hospitality-and-tourism']) }}">School of Hospitality and Tourism</a></li>
            <li><a class="@yield('green-cafe-and-restaurant')" href="{{ route('projects', ['biz'=>'green-cafe-and-restaurant']) }}">Green Cafe and Restaurant</a></li>    -->
        </ul>
    </div>
    <!--End single sidebar-->
    
</div>