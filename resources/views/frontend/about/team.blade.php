@section ('appbottomjs')
@endsection
@section ('home')
@endsection
@section ('content')

 <!--Start team area-->
 <section class="team-area">
        <div class="container">
            <div class="sec-title text-center">
                <h1>T</h1>
                <div class="overlay-title">
                    <h2>Advisor Team</h2> 
                </div>   
            </div>
            @foreach($teams->chunk(5) as $chunk)
            <div class="row">
                <!--Start single item member-->
                @foreach($chunk as $row)
                <div class="col-md-3">
                    <div class="single-team-member">
                        <div class="">
                            <img src="{{ asset($row->image) }}" alt="Awesome Image">
                        </div>
                        <div class="text-holder">
                            <h3>{{$row->name}}</h3>
                            <span>{{$row->position}}</span>
                            <p>{!! $row->description !!}</p>
                            <ul>
                                <li><span class="flaticon-multimedia-1"></span> {{$row->email}} </li>
                            </ul>
                        </div>
                    </div>    
                </div>
                @endforeach

                <div class="col-md-3">
                    <div class="single-team-member">
                        <div class="">
                            <img src="{{ asset($row->image) }}" alt="Awesome Image">
                        </div>
                        <div class="text-holder">
                            <h3>{{$row->name}}</h3>
                            <span>{{$row->position}}</span>
                            <p>{!! $row->description !!}</p>
                            <ul>
                                <li><span class="flaticon-multimedia-1"></span> {{$row->email}} </li>
                            </ul>
                        </div>
                    </div>    
                </div>

                <!--End single item member-->
               
                <!--Start single item member-->
                <!-- <div class="col-md-4">
                    <div class="single-team-member">
                        <div class="img-holder">
                            <img src="{{ asset('public/frontend/images/team/3.jpg') }}" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="content">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-holder">
                            <h3>Darden Eldbridge</h3>
                            <span>HR Manager</span>
                            <p>Circumstances occur in which toil and pain can all procure him some greatest pleasure</p>
                            <ul>
                                <li><span class="flaticon-multimedia-1"></span> contact@atpconsultant.com</li>
                            </ul>
                        </div>
                    </div>    
                </div> -->
                <!--End single item member-->
              
            </div>
            @endforeach
        </div>
    </section>
    <!--End team area-->

    <section class="team-area">
        <div class="container">
            <div class="sec-title text-center">
                <h1>T</h1>
                <div class="overlay-title">
                    <h2>Expert Team</h2> 
                </div>   
            </div>
            @foreach($teams->chunk(5) as $chunk)
            <div class="row">
                <!--Start single item member-->
                @foreach($chunk as $row)
                <div class="col-md-3">
                    <div class="single-team-member">
                        <div class="">
                            <img src="{{ asset($row->image) }}" alt="Awesome Image">
                        </div>
                        <div class="text-holder">
                            <h3>{{$row->name}}</h3>
                            <span>{{$row->position}}</span>
                            <p>{!! $row->description !!}</p>
                            <ul>
                                <li><span class="flaticon-multimedia-1"></span> {{$row->email}} </li>
                            </ul>
                        </div>
                    </div>    
                </div>
                @endforeach

                <div class="col-md-3">
                    <div class="single-team-member">
                        <div class="">
                            <img src="{{ asset($row->image) }}" alt="Awesome Image">
                        </div>
                        <div class="text-holder">
                            <h3>{{$row->name}}</h3>
                            <span>{{$row->position}}</span>
                            <p>{!! $row->description !!}</p>
                            <ul>
                                <li><span class="flaticon-multimedia-1"></span> {{$row->email}} </li>
                            </ul>
                        </div>
                    </div>    
                </div>

                <!--End single item member-->
               
                <!--Start single item member-->
                <!-- <div class="col-md-4">
                    <div class="single-team-member">
                        <div class="img-holder">
                            <img src="{{ asset('public/frontend/images/team/3.jpg') }}" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="content">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-holder">
                            <h3>Darden Eldbridge</h3>
                            <span>HR Manager</span>
                            <p>Circumstances occur in which toil and pain can all procure him some greatest pleasure</p>
                            <ul>
                                <li><span class="flaticon-multimedia-1"></span> contact@atpconsultant.com</li>
                            </ul>
                        </div>
                    </div>    
                </div> -->
                <!--End single item member-->
              
            </div>
            @endforeach
        </div>
    </section>

    @endsection    