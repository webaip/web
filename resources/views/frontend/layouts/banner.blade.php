    
@if(isset($banner['image']))
    <section class="breadcrumb-area" style="background-image: url({{ $banner['image'] }} );">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs">
                        <h1>{{ $banner['title'] }}</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="breadcrumb-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="left pull-left">
                            <ul>
                                <li><a href="index-2.html">Home</a></li>
                                <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                                <li class="active">{{ $banner['title'] }}</li>
                            </ul>
                        </div>
                        <div class="right pull-right">
                            <!-- <a href="#">
                                <span><i class="fa fa-share-alt" aria-hidden="true"></i>Share</span> 
                            </a>    -->
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif
