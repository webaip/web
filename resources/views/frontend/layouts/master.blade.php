<!DOCTYPE html>
<html lang="en">

    <head>
      <meta charset="UTF-8">
      <title>@yield('title')</title>

      <!-- responsive meta -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- For IE -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        
      <!-- master stylesheet -->
      <link rel="stylesheet" href="{{ asset('public/frontend/css/style.css') }}">
      <link rel="stylesheet" href="{{ asset('public/frontend/CamCyber/customized.css') }}">
      <!-- Responsive stylesheet -->
      <link rel="stylesheet" href="{{ asset('public/frontend/css/responsive.css') }}">
        <script src="{{ asset('public/frontend/js/jquery.js') }}"></script>
        <script src="{{ asset('public/frontend/js/bootstrap.min.js') }}"></script>
        <link href=" {{ asset('public/frontend/icon.png') }}" rel="shortcut icon" type="image/png">

    </head>
    <body>


    <!--Start Top bar area -->  
    <section class="top-bar-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="welcome pull-left">
                        <p>ATP, the profestional consulting service provider.</p>
                    </div>
                    <div class="top-menu pull-right">
                       
                        <div class="consultation-button">
                            <a href="{{ route('home','#submit-consultant') }}"><span class="flaticon-multimedia"></span> Free Consultation</a>
                        </div>
                    </div>     
                </div>
            </div>
        </div>
    </section>
    <!--End Top bar area --> 
     
    <!--Start header area-->
    <header class="header-area stricky">
        <!-- <div class="container"> -->
            <div class="row">

                <div class="col-md-12">
                    <div class="logo pull-left">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset(getLogo()) }}" alt="Logo ">
                        </a>
                    </div>
                    <div class="header-right clearfix pull-right">
                        <nav class="main-menu pull-left">
                            <div class="navbar-header">     
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">

                                    <li class="@yield('active-home')"><a href="{{ route('home') }}">Home</a></li>
                                    <li class="dropdown @yield('active-about-us')"><a href="{{ route('about-us') }}">About Us</a>
                                        <ul>
                                            <li><a href="{{ route('teams') }}">Our Team</a></li>
                                            <li><a href="{{ route('expert') }}">Our Compititive Strenght</a></li>
                                            <li><a href="{{ route('award') }}">Our Awards</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown @yield('active-our-businesses')"><a href="#">Our Businesses</a>
                                    
                                        <ul>
                                            <li><a href="{{ route('our-business', ['biz'=>'atp-consultant']) }}">ATP Consultant Co., Ltd</a></li>
                                            <li><a href="{{ route('our-business', ['biz'=>'atp-school-of-hospitality-and-tourism']) }}">ATP School of Hospitality and Tourism</a></li>
                                            <li><a href="{{ route('our-business', ['biz'=>'green-cafe-and-restaurant']) }}">Green Café and Restaurant</a></li>
                                        </ul>

                                    </li>

                                    <li class="dropdown @yield('active-services')" ><a href="{{ route('services') }}">Services</a>

                                        <ul>
                                            <li><a href="{{ route('matching') }}">Job Matching & Coaching/Training</a></li>
                                            <li><a href="{{ route('customize') }}"> Customized & Rebooting Business Training</a></li>
                                            <li><a href="{{ route('bussiness') }}"> Sister Bussiness Training</a></li>
                                        </ul>
                                    
                                    </li>

                                    <li class=" @yield('active-case')" ><a href="{{ route('case') }}">Case Studied</a>

                                    <li class="@yield('active-projects')"><a href="{{ route('projects', ['biz'=>'atp-consultant']) }}">Projects</a></li>
                                    <li class="@yield('active-jobs')"><a href="{{ route('jobs', ['biz'=>'atp-consultant']) }}">Job Opportunities</a></li>
                                    <li class="@yield('active-contact-us')" ><a href="{{ route('contact-us') }}">Contact US</a></li>


                                </ul>
                            </div>
                        </nav>
                       
                    </div>
                    
                </div>
            </div>
        <!-- </div> -->
    </header>  
    <!--End header area-->   


    @yield('content')

      
    <!--Start footer area-->  
    <footer class="footer-area">
        <div class="container">
            <div class="row">
                <!--Start single footer widget-->
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="single-footer-widget mar-btm">
                        <div class="footer-logo">
                            <a href="#">
                                <img src="{{ asset('public/frontend/CamCyber/logo-light.png') }}" alt="ATP Logo">
                            </a>
                        </div>
                        <div class="our-info">
                            <p>{!!getContent('footer-content')!!}</p>
                           
                        </div>
                    </div>
                </div>
                <!--End single footer widget-->
                <!--Start single footer widget-->
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="single-footer-widget mar-btm">
                        <div class="title">
                            <h3>Usefull Links</h3>
                            <span class="border"></span>
                        </div>
                        <ul class="usefull-links">
                            @php( $usefullinks = getusefullLink() )
                            @foreach( $usefullinks as $row )
                                <li><a target="_blank" href="{{$row->url}}">{{$row->title}}</a></li>
                                
                             @endforeach
                             <a href="{{ route('about-us') }}"><li style="color: white;">About Us</li></a>
                             <a href="@yield('active-our-businesses')"><li style="color: white;">Our Businesses</li></a>
                             <a href="{{ route('services') }}"><li style="color: white;">Services</li></a>
                             <a href="{{ route('case') }}"><li style="color: white;">Case Studied</li></a>
                             <a href="@yield('active-projects')"><li style="color: white;">Project</li></a>
                        </ul>
                       
                    </div>
                </div>
                <!--Start single footer widget-->

                <!--Start single footer widget-->
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="single-footer-widget martop">
                        <div class="title">
                            <h3>Contact Details</h3>
                            <span class="border"></span>
                        </div>
                        <ul class="footer-contact-info">
                            <li>
                                <div class="icon-holder">
                                    <span class="flaticon-gps"></span>
                                </div>
                                <div class="text-holder">
                                    <h5>{!!getContent('footer-adress')!!}</h5>
                                </div>
                            </li>
                            <li>
                                <div class="icon-holder">
                                    <span class="flaticon-multimedia-1"></span>
                                </div>
                                <div class="text-holder">
                                    <h5>Street 196, H#25, Sangkat Veal Vong, Khan
                                        Prampi Makara, Phnom Penh of Cambodia</h5>
                                </div>
                            </li>
                            <li>
                                <div class="icon-holder">
                                    <span class="flaticon-technology"></span>
                                </div>
                                <div class="text-holder">
                                    <h5>{!!getContent('footer-phone')!!}</h5>
                                </div>
                            </li>
                            <li>
                                <div class="icon-holder">
                                    <span class="flaticon-multimedia-1"></span>
                                </div>
                                <div class="text-holder">
                                    <h5>{!!getContent('footer-email')!!}</h5>
                                </div>
                            </li>
                        </ul>
                        <ul class="footer-social-links">
                            <li><a target="_blank" href="https://www.facebook.com/ATP.School.Hospitality.Tourism.Cambodia/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <!-- <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li> -->
                            <li><a target="_blank" href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                            <li><a target="_blank" href="#"><img src="{{ asset('public/frontend/images/icon/telegram.png') }}" style="width: 30px;"</a></li>
                        </ul>
                    </div>
                </div>
                <!--End single footer widget-->
            </div>
        </div>
    </footer>   
    <!--End footer area-->

    <!--Start footer bottom area--> 
    <section class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-bottom">
                    <div class="footer-menu pull-left">
                            <ul>
                                <li>Powered By <a target="_blank" href="http://camcyber.com/"> CamCyber</a></li>        
                            </ul>
                        </div>
                        <div class="copyright-text pull-right">
                            <p>Copyrights © <?php echo date("Y"); ?> ATP Consultant. All Rights Reserved. </p> 
                        </div>
                    </div>    
                </div>
            </div>
        </div>    
    </section> 
    <!--End footer bottom area-->  


    <!--Scroll to top-->
    <div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-up"></span></div>

    
    <script src="{{ asset('public/frontend/js/wow.min.js') }}"></script>
    
    <script src="{{ asset('public/frontend/js/jquery.bxslider.min.js') }}"></script>
    <script src="{{ asset('public/frontend/js/jquery.countTo.js') }}"></script>
    <script src="{{ asset('public/frontend/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('public/frontend/js/validation.js') }}"></script>
    <script src="{{ asset('public/frontend/js/jquery.mixitup.min.js') }}"></script>
    <script src="{{ asset('public/frontend/js/jquery.easing.min.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHzPSV2jshbjI8fqnC_C4L08ffnj5EN3A"></script>
    <script src="{{ asset('public/frontend/js/gmaps.js') }}"></script>
    <script src="{{ asset('public/frontend/js/map-helper.js') }}"></script>
    <script src="{{ asset('public/frontend/js/jquery.fitvids.js') }}"></script>
    <script src="{{ asset('public/frontend/assets/jquery-ui-1.11.4/jquery-ui.js') }}"></script>
    <script src="{{ asset('public/frontend/assets/language-switcher/jquery.polyglot.language.switcher.js') }}"></script>
    <script src="{{ asset('public/frontend/js/jquery.fancybox.pack.js') }}"></script>
    <script src="{{ asset('public/frontend/js/jquery.appear.js') }}"></script>
    <script src="{{ asset('public/frontend/js/isotope.js') }}"></script>
    <script src="{{ asset('public/frontend/js/jquery.prettyPhoto.js') }}"></script> 
    <script src="{{ asset('public/frontend/assets/timepicker/timePicker.js') }}"></script>
    <script src="{{ asset('public/frontend/assets/bootstrap-sl-1.12.1/bootstrap-select.js') }}"></script>                               

    <!-- revolution slider js -->
    <script src="{{ asset('public/frontend/assets/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('public/frontend/assets/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('public/frontend/assets/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
    <script src="{{ asset('public/frontend/assets/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
    <script src="{{ asset('public/frontend/assets/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script src="{{ asset('public/frontend/assets/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script src="{{ asset('public/frontend/assets/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
    <script src="{{ asset('public/frontend/assets/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script src="{{ asset('public/frontend/assets/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
    <script src="{{ asset('public/frontend/assets/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script src="{{ asset('public/frontend/assets/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
    <!-- thm custom script -->
    <script src="{{ asset('public/frontend/js/custom.js') }}"></script>


    </body>

</html>