@extends('frontend.layouts.master')

@section('title', 'News | '.env('APP_NAME'))

@section(last(request()->segments()), 'active')

@section ('content')
@php($banner = getBanner(5))
    @if($banner)
        @include('frontend.layouts.banner', ['banner'=>['image'=>asset($banner->banner->image), 'title'=>'News']])
    @endif 


<div class="container"  style="padding-top: 500px;">
    <div class="row" >
        <div class="col-md-9">
            <div class="title" style=" margin-top: -454px;  background: #80808012;   width: 802px;height: 591px;">
                <h2 style="margin-top: 28px;margin-bottom: 53px; color: #1e72bd;">Customized & Rebooting Business Training</h2>
                <h4 style="padding: 5px;margin-top: 18px; padding-left: 17px; color: #1e72bd;">Customized</h4>
                <h4 style="padding: 5px;margin-top: 18px; padding-left: 17px; color: #1e72bd;">Rebooting Business Training</h4>
                <h4 style="padding: 5px;margin-top: 18px; padding-left: 17px; color: #1e72bd;">Case Studied</h4>
            </div>
            <div class="title-2" style=" margin-top: -454px; width: 802px;height: 591px;">
                <h2 style="margin-top: 108px;margin-bottom: 53px; color: #1e72bd;">MSME Business </h2>
                <h4 style="padding: 5px;margin-top: 18px; padding-left: 17px; color: #1e72bd;">Skills training </h4>
                <h4 style="padding: 5px;margin-top: 18px; padding-left: 17px; color: #1e72bd;">Entrepreneurship soft skills training</h4>
                <h4 style="padding: 5px;margin-top: 18px; padding-left: 17px; color: #1e72bd;">Case Studied</h4>
            </div>
        </div>

        <div class="col-md-3">
                <div class="title" style="color: #1e72bd; background: #8080800a; height: 620px; width: 317px; margin-top: -427px;">
                    <h2 style="text-align: center; margin-top: 1px; padding: 10px;color: #1e72bd;">Services</h2>
                    <div class="link" style="color: #1e72bd;">
                        <h4 style="padding: 5px;margin-top: 18px; padding-left: 17px;">Job Matching & Coaching</h4>
                        <h4 style="padding: 5px;margin-top: 18px; padding-left: 17px;">Training & Customized  
                        <h4 style="padding: 5px;margin-top: 18px; padding-left: 17px;">Rebooting Business Training</h4>
                        <h4 style="padding: 5px;margin-top: 18px; padding-left: 17px;">& MSME Bussiness startups</h4>
                    </div>   
                    <div class="text">
                        <h2 style="text-align: center; margin-top: 22px; margin-right: 10px;color: #1e72bd;">Contact Us</h2>                    
                        <p style="margin-top: 16px; padding-left: 17px;color:black;">Email: info@atpconsultant.com</p>
                        <p style="padding-left: 17px;color:black;">Mobile: 010 696 099 | 012 398 353</p>
                        <p style="padding-left: 17px;color:black;">Office 1:</p>
                        <p style="padding-left: 17px;color:black;">Street 150, H#166, Sangkat Teuk LaakII, Khan Toul Kork, Phnom Penh of Cambodia</p>
                    </div>
                    <div class="icon" style="text-align: center;">
                        <i class="fa fa-phone" aria-hidden="true" style="height: 30px; width: 30px;"></i>
                        <i class="fa fa-linkedin" aria-hidden="true" style="height: 30px; width: 30px;"></i>
                        <a href="https://www.facebook.com/atpconsultant"><i class="fa fa-facebook-official" aria-hidden="true" style="height: 30px; width: 30px;"></i></a>
                        
                    </div>   
                </div>
            </div>

    </div>
</div>


    @endsection    