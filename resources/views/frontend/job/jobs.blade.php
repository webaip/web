@extends('frontend.layouts.master')

@section('title', 'Jobs | '.env('APP_NAME'))
@section('active-jobs', 'current')
@section(last(request()->segments()), 'active')

@section ('content')
    @php($banner = getBanner(9))
    @if($banner)
        @include('frontend.layouts.banner', ['banner'=>['image'=>asset($banner->banner->image), 'title'=>'Job Opportunities']])
    @endif 

    <section id="blog-area" class="blog-large-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-5 col-sm-7 col-xs-12 pull-left">
            @php($route = last(request()->segments()))
                @include('frontend.job.menu')
                
            </div>  
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                @if($data)
                <div class="blog-post">
                    @foreach($data as $row)
                    <!--Start single blog post-->
                    <div class="single-blog-post">
                        <div class="img-holder">
                            <img src="{{ asset($row->image) }}" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="content">
                                        <a href="{{ route('job-detail',['biz'=>$route,'slug'=>$row->slug]) }}"><i class="fa fa-link" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-holder">
                            <ul class="meta-info">
                                <li><a href="#">Deadline: {{ Carbon\Carbon::parse($row->closed_date)->format('d') }}-{{ Carbon\Carbon::parse($row->closed_date)->format('F') }}-{{ Carbon\Carbon::parse($row->closed_date)->format('Y') }}</a></li>
                            </ul>
                            <a href="{{ route('job-detail',['biz'=>$route,'slug'=>$row->slug]) }}">
                                <h3 class="blog-title">{{$row->position}}</h3>
                            </a> 
                            <div class="text">
                                <p>{{$row->description}}</p>
                            </div>
                            <div class="bottom clearfix">
                                <div class="readmore pull-left">
                                    <a href="{{ route('job-detail',['biz'=>$route,'slug'=>$row->slug]) }}">Read More</a>    
                                </div>
                                <div class="comment pull-right">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End single blog post-->
                    @endforeach
                    
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {{ $data->links('vendor.pagination.frontend-html') }}
                    </div>
                </div>
                @else
                    <p>No Data</p>
                @endif
            </div>
             
        </div>
    </div>
</section>





@endsection