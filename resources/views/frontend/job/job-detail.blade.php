@extends('frontend.layouts.master')

@section('title', 'Job Detail | '.env('APP_NAME'))
@section('active-jobs', 'current')
@section(request()->segments()['1'], 'active')

@section ('content')
    @if($data)
        @include('frontend.layouts.banner', ['banner'=>['image'=>asset($data->image), 'title'=>$data->position]])
    @endif
    <section id="blog-area" class="blog-large-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-5 col-sm-7 col-xs-12 pull-left">
                
                @include('frontend.job.menu')
                
            </div>  
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
               
                <div class="blog-post">
                    <div class="single-blog-post">
                        
                        <div class="text-holder">
                            <ul class="meta-info">
                                <li><a href="#">Deadline</a></li>
                                <li>
                                    <a href="#"> 
                                    @if($data)
                                    {{ Carbon\Carbon::parse($data->closed_date)->format('d') }}-{{ Carbon\Carbon::parse($data->closed_date)->format('F') }}-{{ Carbon\Carbon::parse($data->closed_date)->format('Y') }}
                                    @endif
                                   </a>
                                   </li>
                            </ul>
                            <h3 class="blog-title">@if($data) {{$data->position}} @endif</h3>
                            <br>
                            <h3 style="font-size:18px;" class="blog-title"> <i class="fa fa-map-marker" aria-hidden="true"></i> Location  </h3>
                            <div class="text">
                                
                                <p class="mar-bottom">@if($data) {{$data->location}} @endif</p>
                                
                            </div>
                            <h3 style="font-size:18px;" class="blog-title"> <i class="fa fa-bookmark" aria-hidden="true"></i> Description</h3>
                            <div class="text">
                                
                                <p class="mar-bottom">@if($data) {{$data->description}} @endif</p>
                               
                            </div>
                            <h3 style="font-size:18px;" class="blog-title"> <i class="fa fa-list" aria-hidden="true"></i> Requirment</h3>
                            <div class="text">
                                
                                <p class="mar-bottom">@if($data) {!!$data->requirment!!} @endif</p>
                               
                            </div>
                        </div>
                    </div>
                   
                
                
                </div>

              
            </div>
           
        </div>
    </div>
</section>





@endsection