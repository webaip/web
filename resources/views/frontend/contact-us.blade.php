@extends('frontend.layouts.master')

@section('title', 'Contact Us | '.env('APP_NAME'))
@section('active-contact-us', 'current')

@section ('appbottomjs')
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyBbz45_RGsB8xrJtKSgdnL8jJTw0dX-nNw"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section ('content')
    @php($banner = getBanner(10))
    @if($banner)
        @include('frontend.layouts.banner', ['banner'=>['image'=>asset($banner->banner->image), 'title'=>'Contact Us']])
    @endif 


     <!--Start contact info area-->
<section class="contact-info-area">
    <div class="container">
        <div class="row">
           
            
            <div class="col-md-4">
                <div class="contact-author-info">
                    <div class="title">
                        <h3>Human Resource:</h3>
                        <span class="border"></span>
                    </div>
                    @foreach($human_resources as $row)
                    <ul class="mar-btm">
                        <li>
                            <div class="img-holder">
                                <img src="{{ asset($row->image) }}" alt="">
                            </div>
                            <div class="text-holder">
                                <h5>{{$row->name}}</h5>
                               <p><i class="fa fa-phone" aria-hidden="true"></i> {{$row->phone}}</p>
                                        <p><i class="fa fa-envelope" aria-hidden="true"></i> {{$row->email}}</p>
                            </div>
                        </li>
                    </ul>
                    @endforeach
                </div>        
            </div>
            <div class="col-md-4">
                <div class="work-box">
                    <div class="title">
                        <h3>Working Hours</h3>
                        <span class="border"></span>
                    </div>
                    <ul class="working-hours">
                        @foreach($working_hours as $row)
                            <li class="bg-white">{{$row->day}} <span>{{$row->time}}</span></li>
                        @endforeach   
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="contact-info-carousel">
                    <!--Start single box-->
                    @foreach($contact_address as $row)
                    <div class="single-box">
                        <div class="title">
                            <h3>{{$row->name}}</h3>
                            <span class="border"></span>
                        </div>
                        <ul class="contact-info-list">
                            <li>
                                <div class="icon-holder">
                                    <span class="flaticon-gps"></span>
                                </div>
                                <div class="text-holder">
                                    <h5><span>Address:</span> {{$row->address}}</h5>
                                </div>
                            </li>
                            <li>
                                <div class="icon-holder">
                                    <span class="flaticon-gps"></span>
                                </div>
                                <div class="text-holder">
                                    <h5><span>Postal Address:</span>Street 196, H#25, Sangkat Veal Vong, Khan
                                        Prampi Makara, Phnom Penh of Cambodia</h5>
                                </div>
                            </li>
                            <li>
                                <div class="icon-holder">
                                    <span class="flaticon-technology"></span>
                                </div>
                                <div class="text-holder">
                                    <h5><span>Call Us:</span> <br>{{$row->phone}}</h5>
                                </div>
                            </li>
                            <li>
                                <div class="icon-holder">
                                    <span class="flaticon-multimedia-1"></span>
                                </div>
                                <div class="text-holder">
                                    <h5><span>Mail Us:</span> <br>{{$row->email}}</h5>
                                </div>
                            </li>
                        </ul>
                        
                    </div>
                    @endforeach
                    <!--End single box-->
                
                </div>
            </div>
        </div>
    </div>
</section>
<!--End contact info area--> 

<div class="tp-parallax-wrap visible-lg">
        <div class="tp-parallax-wrap sreen-responsive" style="position: absolute;top: 227px; z-index: 17;left:140px;">
            <div class="tp-loop-wrap">
                <div class="tp-mask-wrap">
                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme tp-static-layer" id="slider-1-layer-26" data-x="['right','right','center','left']" data-hoffset="['20','20','-4','50']" data-y="['middle','top','top','top']" data-voffset="['19','195','513','300']" data-width="['351','343','393','378']" data-height="['451','437','439','407']" data-whitespace="nowrap" data-type="shape" data-responsive_offset="on" data-startslide="0" data-endslide="2" data-frames="[{&quot;delay&quot;:0,&quot;speed&quot;:300,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]" data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 17; font-family: &quot;Open Sans&quot;; visibility: inherit; transition: none 0s ease 0s; text-align: inherit; line-height: 24px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 400; font-size: 15px; white-space: nowrap; min-height: 451px; min-width: 351px; max-height: 451px; max-width: 351px; opacity: 1; transform-origin: 50% 50% 0px; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);left:300;"> 
                       <div class="slide-menu">
                           <h3>Our Business</h3>
                           <div class="biz-cnt">
                               <a href= "{{ route('our-business', ['biz'=>'atp-consultant']) }}">
                                    <div class="item">
                                       ATP Consultant Co., Ltd
                                   </div>
                                <a>
                                
                                <a href= "{{ route('our-business', ['biz'=>'atp-school-of-hospitality-and-tourism']) }}">
                               <div class="item">
                                   ATP School of Hospitality and Tourism
                               </div>
                               <a>

                               <a href= "{{ route('our-business', ['biz'=>'green-cafe-and-restaurant']) }}">
                               <div class="item">
                                 Green Café and Restaurant
                               </div>
                               <a>
                           </div>
                       </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--Start contact area-->
<section id="send-contact" class="contact-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="contact-form">
                    <div class="sec-title text-center">
                        <h1>S</h1>
                        <div class="overlay-title">
                            <h2>Send Message Us</h2> 
                        </div>   
                    </div>
                    @if(Session::has('msg'))
                    <div class="alert alert-success" role="alert">
                        Your request has been sent! We will respone you soon.
                    </div>
                    @endif
                    @if (count($errors) > 0)
                    <div class="alert alert-danger" role="alert">
                            Sending fail! Please Try again!
                        </div>
                    @endif
                    

                    @if (count($errors) > 0)
                        
                        @foreach ($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">
                            {{ $error }}
                        </div>
                        @endforeach
                    @endif
                    <form id="contact-form" name="contact_form" class="default-form" action="{{ route('submit-contact') }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" name="name" value="" placeholder="Your Name*" required="">
                            </div>
                            <div class="col-md-6">
                                <input type="email" name="email" value="" placeholder="Your Mail*" required="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" name="phone" value="" placeholder="Phone">
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="subject" value="" placeholder="Subject">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <textarea name="message" placeholder="Your Message.." required=""></textarea>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="">
                                <button class="thm-btn bg-1" type="submit" data-loading-text="Please wait...">send message</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End contact area--> 

<!--Start Google map area-->
<section class="contact-area">
            <iframe
              width="100%"
              height="450"
              frameborder="0" style="border:0"
              src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCOVmFlwfcjVJE1mgzI69HIOnIwLYEW1OM
                &q=%ATP+School+of+Hospitality+and+Tourism" allowfullscreen>
            </iframe>
</section>
<!--End Google map area--> 





@endsection