@extends('frontend.layouts.master')

@section('title', 'About Us | '.env('APP_NAME'))
@section('active-about-us', 'current')


@section ('content')
    @php($banner = getBanner(1))
    @if($banner)
        @include('frontend.layouts.banner', ['banner'=>['image'=>asset($banner->banner->image), 'title'=>'About Us']])
    @endif



    <div class="container">
        <div class="row" style="margin-top: 52px;">
            <div class="col-md-4">
                <img src="{{ asset('public/frontend/images/projects/award-1.jpg') }}" style="padding-left: 60px; width: 330px; height: 281px;">
                <p style="padding-left: 101px;">Great explorer of the truth,</p>
                <p style="padding-left: 59px; margin-top: -8px;}"> the master-builder of human happiness.</p>
            </div>
            <div class="col-md-4">
                <img src="{{ asset('public/frontend/images/projects/award-2.jpg') }}" style="width: 324px; height: 281px;">
                <p>Great explorer of the truth, the master-builder of human happiness.</p>
            </div>
            <div class="col-md-4">
                <img src="{{ asset('public/frontend/images/projects/award-3.jpg') }}" style="width: 324px; height: 281px; margin-left: 0px;">
                <p>Great explorer of the truth, the master-builder of human happiness.</p>
            </div>
        </div>
    
    </div>

    @endsection    