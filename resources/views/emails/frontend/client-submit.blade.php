@component('mail::message')
@if($data['is_sent_to_client'] == 1)
# Thanks for contacting GEOLINK LOGISTIC.
We have receive your message through our website contact form. Our staff wiil check and respone to you soon.
<br/> Have a nice day!  
<br/>{{ config('app.name') }}
@else
# Dear Admin! 
There was a customer contacting us through our website contact form. Please review the following information.

@component('mail::table')
|          		  |    			|
| --------------- |:-------------:|
| Name         	  | {{ $data['name'] }} 	 			| 
| Subject    	  | {{ $data['subject'] }} 	 			|
| Email       	  | {{ $data['email'] }} 	 			|
| Message         | {{ $data['message'] }}  	 			|
@endcomponent

<br/> Have a nice day!  
<br/>{{ config('app.name') }}
@endif


@endcomponent
