<?php

namespace App\Http\Controllers\CP\Consultant;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\Consultation as Model;

class ConsultantController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "cp.consultant";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

     public function index(){
        $data = Model::select('*')->orderBy('id','DESC');
        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $key       =   isset($_GET['key'])?$_GET['key']:"";
        $from=isset($_GET['from'])?$_GET['from']:"";
        $till=isset($_GET['till'])?$_GET['till']:"";
        $appends=array('limit'=>$limit);
        if( $key != "" ){
            $data = $data->where('name', 'like', '%'.$key.'%')->orWhere('email', 'like', '%'.$key.'%')->orWhere('subject', 'like', '%'.$key.'%');
            $appends['key'] = $key;
        }
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $till]);
            }
        }
        $data= $data->orderBy('id', 'DESC')->paginate($limit);
        
        return view($this->route.'.index', ['route'=>$this->route, 'data'=>$data,'appends'=>$appends]);
    }
   
    public function create(){
        return view($this->route.'.create' , ['route'=>$this->route]);
    }
    public function store(Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'name' =>   $request->input('name'), 
                    'phone' =>  $request->input('phone'),
                    'email' =>  $request->input('email'),
                    'subject' =>  $request->input('subject'),
                    'message' =>  $request->input('message'),
                   
                    'created_at' => $now
                );
        
        Session::flash('invalidData', $data );
        Validator::make(
                        $data, 
                        [
                            
                           'name' => 'required',
                           'subject' => 'required',
                           'phone' => [
                                            'required',
                                            Rule::unique('messages')
                                        ],
                            'email' => [
                                            'required',
                                            'email',
                                            Rule::unique('messages')
                                        ]
                        ])->validate();
        
        
		$id=Model::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
		return redirect(route($this->route.'.edit', $id));
    }

    public function edit($id = 0){
        $this->validObj($id);
        $data = Model::find($id);
        return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function update(Request $request){
        $id = $request->input('id');
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'name' =>   $request->input('name'), 
                    'phone' =>  $request->input('phone'),
                    'email' =>  $request->input('email'),
                    'subject' =>  $request->input('subject'),
                    'message' =>  $request->input('message'),
                    
                    'updated_at' => $now
                );
        

        Validator::make(
        				$data, 
			        	[
                            
                            'name' => 'required',
                            'subject' => 'required',
                            'phone' => [
                                            'required',
                                            Rule::unique('messages')
                                        ],
                            'email' => [
                                            'required',
                                            'email',
                                            Rule::unique('messages')
                                        ]
						])->validate();
        
        
        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
	}

     public function trash($id){
        Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Data has been deleted'
        ]);
    }
    function status(Request $request){
      $id   = $request->input('id');
      $data = array('is_published' => $request->input('status'));
      Model::where('id', $id)->update($data);
      return response()->json([
          'status' => 'success',
          'msg' => 'Member status has been updated.'
      ]);
    }

}
