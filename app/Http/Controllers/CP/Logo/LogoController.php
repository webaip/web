<?php

namespace App\Http\Controllers\CP\Logo;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Session;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\Logo as Model;


class LogoController extends Controller
{
    
  protected $route; 
  public function __construct(){
      $this->route = "cp.logo";
  }
  function validObj($id=0){
      $data = Model::find($id);
      if(empty($data)){
         echo "Invalide Object"; die;
      }
  }

   
    public function index(){
      $data = Model::select('*')->orderBy('id','ASC')->get();
      
      return view($this->route.'.index', ['route'=>$this->route, 'data'=>$data]);
  }

    public function create(){
      return view($this->route.'.create' , ['route'=>$this->route]);
  }

  public function store(Request $request) {
    $logo_id    = Auth::id();
    $now        = date('Y-m-d H:i:s');

    $data = array(
                'creator_id' => $logo_id,
                'created_at' => $now
            );
    
    Session::flash('invalidData', $data );
    Validator::make(
                    $data, 
                    [
                        
                       'image' => [
                                        'mimes:jpeg,png,jpg',
                        ],
                    ])->validate();
    $image = FileUpload::uploadFile($request, 'image', 'uploads/logo');
    if($image != ""){
        $data['image'] = $image; 
    }
    if($request->input('status')=="")
    {
        $data['is_published']=0;
    }else{
        $data['is_published']=1;
    }
$id=Model::insertGetId($data);
    Session::flash('msg', 'Data has been Created!');
return redirect(route($this->route.'.edit', $id));
}

public function edit($id = 0){
  $this->validObj($id);
  $data = Model::find($id);
  return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
}

    public function showEditForm($id = 0){   
      $data = Model::where('id', $id)->first();
      if(!empty($data)){
        return view('cp.submit-form.edit', ['route'=>$this->route,'data'=>$data]);
      }else{
        return response(view('errors.404'), 404);
      }
    }

    public function update(Request $request){
      $id = $request->input('id');
      $logo_id    = Auth::id();
      $now        = date('Y-m-d H:i:s');

      $data = array(
                 
                  'updater_id' => $logo_id,
                  'updated_at' => $now
              );
      

      Validator::make(
              $data, 
              [
                          
                          'image' => [
                                          'mimes:jpeg,png,jpg',
                          ],
          ])->validate();

      $image = FileUpload::uploadFile($request, 'image', 'uploads/logo');
      if($image != ""){
          $data['image'] = $image; 
      }
      
      Model::where('id', $id)->update($data);
      Session::flash('msg', 'Data has been updated!' );
      return redirect()->back();
}


  function updateStatus(Request $request){
    $id   = $request->input('id');
    // $data = array('is_published' => $request->input('active'));
    Model::where('id', $id)->update($data);
    return response()->json([
        'status' => 'success',
        'msg' => 'Published status has been updated.'
    ]);
  }

   
   
}
