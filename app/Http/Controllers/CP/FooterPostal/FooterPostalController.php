<?php

namespace App\Http\Controllers\CP\FooterPostal;

use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;

use App\Model\FooterPostal as Model;



class FooterPostalController extends Controller
{
    
    function __construct (){
       $this->route = "cp.footer-postal.footer-postal";
    }

    public function listData($page = ""){   
      $data = Model::where('page', $page)->get();
      if(!empty($data)){
        return view($this->route.'.list', ['route'=>$this->route,'page' => $page, 'data'=>$data]);
      }else{
        return response(view('errors.404'), 404);
      }
    }
    public function showEditForm($slug = ""){   
      $data = Model::where('slug', $slug)->first();
      if(!empty($data)){
        return view($this->route.'.editForm', ['route'=>$this->route,'data'=>$data]);
      }else{
        return response(view('errors.404'), 404);
      }
    }
    
    public function update(Request $request){
        
      $id = $request->input('id');
      $slug = $request->input('slug');
      $image = "";
      $validate = array(

                      'footer-postal' => 'required',
                      //'name' => 'required'
                  );
      $data = array(
                  //'name' =>   $request->input('name'),
                  'footer-postal' =>   $request->input('footer-postal')
              );

      if($request->input('image_required')){
          $validate['image'] = array( 'sometimes',
                                      'required',
                                      'mimes:jpeg,png'
                                      );
          
          
      }
      //print_r($validate); die;
      Validator::make($request->all(), $validate)->validate();
  
      if($request->input('image_required')){
          $image = FileUpload::uploadFile($request, 'image', 'uploads/footer-postal');
          if($image != ""){
              $data['image'] = $image; 
          }
      }

      Model::where('id', $id)->update($data);
      Session::flash('msg', 'Data has been updated!' );
      return redirect()->back();

    }

   
    
}
