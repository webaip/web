<?php

namespace App\Http\Controllers\CP\ContactAddress;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\ContactAddress as Model;
use App\Model\Service as Service;


class ContactAddressController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "cp.contact_address";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide OurBussiness"; die;
        }
    }

    public function index(){
        $data = Model::select('*')->orderBy('id','DESC')->get();
        
        return view($this->route.'.index', ['route'=>$this->route, 'data'=>$data]);
    }
   
    
    public function edit($id = 0){
        $this->validObj($id);
        $services = Service::get();
        $data = Model::find($id);
        return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data, 'services' => $services]);
    }

    public function update(Request $request){
        $id = $request->input('id');
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'name' =>   $request->input('name'),
                    'address' =>   $request->input('address'),
                    'email' =>   $request->input('email'),
                    'phone' =>   $request->input('phone'),
                    'updater_id' => $user_id,
                    'updated_at' => $now
                );
        

        Validator::make(
                        $data, 
                        [
                            
                            'name' => 'required',
                            'address' => 'required',
                            'email' => 'required',
                            'phone' => 'required',
                        ])->validate();

        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
    }

    
}
