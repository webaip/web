<?php

namespace App\Http\Controllers\CP\News;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;
use App\Http\Controllers\CamCyber\GenerateSlugController as GenerateSlug;

use App\Model\News as Model;
use App\Model\Service as Service;


class NewsController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "cp.news";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function index(){
        $data = Model::select('*');
        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $key       =   isset($_GET['key'])?$_GET['key']:"";
        $from=isset($_GET['from'])?$_GET['from']:"";
        $till=isset($_GET['till'])?$_GET['till']:"";
        $appends=array('limit'=>$limit);
        if( $key != "" ){
            $data = $data->where('title', 'like', '%'.$key.'%');
            $appends['key'] = $key;
        }
       
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $till]);
            }
        }
        $data= $data->orderBy('data_order','ASC')->paginate($limit);
        return view($this->route.'.index', ['route'=>$this->route, 'data'=>$data,'appends'=>$appends]);
    }
    function order(Request $request){
       $string = $request->input('string');
       $data = json_decode($string);
       //print_r($data); die;
        foreach($data as $row){
            Model::where('id', $row->id)->update(['data_order'=>$row->order]);
        }
       return response()->json([
          'status' => 'success',
          'msg' => 'Data has been ordered.'
      ]);
    }
    public function create(){
        $services = Service::get();
        return view($this->route.'.create' , ['route'=>$this->route, 'services' => $services]);
    }
    public function store(Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'title'         => $request->input('title'),
                    'service_id'    => $request->input('service_id'),
                    'slug'      =>   GenerateSlug::generateSlug('news', $request->input('title')),
                    'description' =>   $request->input('description'),
                    'content' =>   $request->input('content'),
                    'creator_id' => $user_id,
                    'created_at' => $now
                );
        
        Session::flash('invalidData', $data );
        Validator::make(
                        $data, 
                        [
                            
                           'title' => 'required',
                           'service_id' => 'required',
                           'image' => [
                                            'mimes:jpeg,png,jpg',
                            ],
                            'feature_image' => [
                                            'mimes:jpeg,png,jpg',
                            ],
                        ])->validate();
        $feature_image = FileUpload::uploadFile($request, 'feature_image', 'uploads/news/feature');
        if($feature_image != ""){
            $data['feature_image'] = $feature_image; 
        }
        $image = FileUpload::uploadFile($request, 'image', 'uploads/news');
        if($image != ""){
            $data['image'] = $image; 
        }
       
        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }
        if($request->input('featured')=="")
        {
            $data['is_featured']=0;
        }else{
            $data['is_featured']=1;
        }
		$id=Model::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
		return redirect(route($this->route.'.edit', $id));
    }

    public function edit($id = 0){
        $this->validObj($id);
        $services = Service::get();
        $data = Model::find($id);
        return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data, 'services' => $services]);
    }

    public function update(Request $request){
        $id = $request->input('id');
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'title'         =>   $request->input('title'),
                    'service_id'    =>   $request->input('service_id'),
                    'slug'          =>   GenerateSlug::generateSlug('news', $request->input('title')),
                    'description'   =>   $request->input('description'),
                    'content'       =>   $request->input('content'),
                    'updater_id'    =>   $user_id,
                    'updated_at'    =>   $now
                );
        

        Validator::make(
        				$data, 
			        	[
                            
                            'title' => 'required',
                            'service_id' => 'required',
                            'image' => [
                                            'mimes:jpeg,png,jpg',
                            ],
                            'feature_image' => [
                                            'mimes:jpeg,png,jpg',
                            ],
						])->validate();
        $feature_image = FileUpload::uploadFile($request, 'feature_image', 'uploads/news/feature');
        if($feature_image != ""){
            $data['feature_image'] = $feature_image; 
        }

        $image = FileUpload::uploadFile($request, 'image', 'uploads/news');
        if($image != ""){
            $data['image'] = $image; 
        }

        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }
        if($request->input('featured')=="")
        {
            $data['is_featured']=0;
        }else{
            $data['is_featured']=1;
        }
        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
	}

     public function trash($id){
        Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Data has been deleted'
        ]);
    }

    function updateStatus(Request $request){
      $id   = $request->input('id');
      $data = array('is_published' => $request->input('active'));
      Model::where('id', $id)->update($data);
      return response()->json([
          'status' => 'success',
          'msg' => 'Published status has been updated.'
      ]);
    }

}
