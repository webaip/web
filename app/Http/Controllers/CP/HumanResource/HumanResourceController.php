<?php

namespace App\Http\Controllers\CP\HumanResource;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\HumanResource as Model;
use App\Model\Service as Service;


class HumanResourceController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "cp.human_resource";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide OurBussiness"; die;
        }
    }

    public function index(){
        $data = Model::select('*')->orderBy('id','DESC')->get();
        return view($this->route.'.index', ['route'=>$this->route, 'data'=>$data]);
    }
    function order(Request $request){
       $string = $request->input('string');
       $data = json_decode($string);
       //print_r($data); die;
        foreach($data as $row){
            Model::where('id', $row->id)->update(['data_order'=>$row->order]);
        }
       return response()->json([
          'status' => 'success',
          'msg' => 'Data has been ordered.'
      ]);
    }
   
    public function edit($id = 0){
        $this->validObj($id);
        $services = Service::get();
        $data = Model::find($id);
        return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data, 'services' => $services]);
    }

    public function update(Request $request){
        $id = $request->input('id');
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'name' =>   $request->input('name'),
                    'email' =>   $request->input('email'),
                    'phone' =>   $request->input('phone'),
                    'updater_id' => $user_id,
                    'updated_at' => $now
                );
        

        Validator::make(
                        $data, 
                        [
                            'name' => 'required',
                            'email' => 'required',
                            'phone' => 'required',
                            'image' => [
                                            'mimes:jpeg,png,jpg',
                            ],
                        ])->validate();

        $image = FileUpload::uploadFile($request, 'image', 'uploads/human_resource');
        if($image != ""){
            $data['image'] = $image; 
        }
        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
    }

    public function trash($id){
        Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Data has been deleted'
        ]);
    }

    
}
