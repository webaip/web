<?php

namespace App\Http\Controllers\CP\AboutUs;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\AboutUs as Model;
use App\Model\Service as Service;


class AboutUsController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "cp.about_us";
    }
   
    public function index(){
        $data = Model::find(1);
        if($data){
            return view($this->route.'.edit', ['route'=>$this->route, 'data'=>$data]);
        }else{
            echo '404';die;
        }
        
    }
   
   
    // public function edit($id = 0){
    //     $this->validObj($id);
    //     $services = Service::get();
    //     $data = Model::find($id);
    //     return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data, 'services' => $services]);
    // }

    public function update(Request $request){
        $id = $request->input('id');
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'title' =>   $request->input('title'),
                    'description' =>   $request->input('description'),
                    'ceo_name' =>   $request->input('ceo_name'),
                    'ceo_position' =>   $request->input('ceo_position'),
                    'mission' =>   $request->input('mission'),
                    'vision' =>   $request->input('vision'),
                    'value' =>   $request->input('value'),
                    'updater_id' => $user_id,
                    'updated_at' => $now
                );
        

        Validator::make(
                        $data, 
                        [
                            'title' => 'required',
                            'description' => 'required',
                            'ceo_name' => 'required',
                            'ceo_position' => 'required',
                            'mission' => 'required',
                            'vision' => 'required',
                            'value' => 'required',
                            'image' => [
                                            'mimes:jpeg,png,jpg',
                            ],
                        ])->validate();

        $image = FileUpload::uploadFile($request, 'image', 'uploads/our_bussiness');
        if($image != ""){
            $data['image'] = $image; 
        }
        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
    }

    
}
