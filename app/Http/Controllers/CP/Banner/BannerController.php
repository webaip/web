<?php

namespace App\Http\Controllers\CP\Banner;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\Banner as Model;

use App\Model\Page as Page;

class BannerController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "cp.banner";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function index(){
        $data = Model::select('*')->get();
        
        return view($this->route.'.index', ['route'=>$this->route, 'data'=>$data]);
    }
   
    public function create(){
        return view($this->route.'.create' , ['route'=>$this->route]);
    }
    public function store(Request $request) {
        $banner_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'title' =>   $request->input('title'),
                    'creator_id' => $banner_id,
                    'created_at' => $now
                );
        
        Session::flash('invalidData', $data );
        Validator::make(
                        $data, 
                        [
                            
                           'title' => 'required',
                           'image' => [
                                            'mimes:jpeg,png,jpg',
                            ],
                        ])->validate();
        $image = FileUpload::uploadFile($request, 'image', 'uploads/banner');
        if($image != ""){
            $data['image'] = $image; 
        }
        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }
		$id=Model::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
		return redirect(route($this->route.'.edit', $id));
    }

    public function edit($id = 0){
        $this->validObj($id);
        $data = Model::find($id);
        return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function update(Request $request){
        $id = $request->input('id');
        $banner_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'title' =>   $request->input('title'), 
                    'updater_id' => $banner_id,
                    'updated_at' => $now
                );
        

        Validator::make(
        				$data, 
			        	[
                            
                            'title' => 'required',
                            'image' => [
                                            'mimes:jpeg,png,jpg',
                            ],
						])->validate();

        $image = FileUpload::uploadFile($request, 'image', 'uploads/banner');
        if($image != ""){
            $data['image'] = $image; 
        }
        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }
        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
	}

     public function trash($id){
        Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Data has been deleted'
        ]);
    }

    public function page($id=0){
        $pages = Page::get();
        $data = Model::find($id)->pageBanners;
        return view($this->route.'.page', ['route'=>$this->route, 'id'=>$id, 'data'=>$data, 'pages'=>$pages]);
    }
    public function checkPages($id=0){
        $banner_id        = $_GET['banner_id'];
        $page_id   = $_GET['page_id'];
        $now            = date('Y-m-d H:i:s');

        $slide = Model::find($banner_id);
        $dataPermision = $slide->pageBanners()->select('page_id')->get(); 

        $is_permision_existed = 0;
        foreach($dataPermision as $row){
            if($row->page_id == $page_id){
                $is_permision_existed = 1;
            }
        }
       
        if($is_permision_existed == 1){
            $slide->pageBanners()->where('page_id', $page_id)->delete();
            return response()->json([
                  'status' => 'success',
                  'msg' => 'Page has been removed.'
            ]);
        }else{
            $data_permision=array(
                'banner_id'  => $banner_id,
                'page_id'    => $page_id,
                'created_at' => $now, 
                'updated_at' => $now
                );
            $slide->pageBanners()->insert($data_permision);
             return response()->json([
                  'status' => 'success',
                  'msg' => 'Page has been added.'
              ]);
        }
    }

    function updateStatus(Request $request){
      $id   = $request->input('id');
      $data = array('is_published' => $request->input('active'));
      Model::where('id', $id)->update($data);
      return response()->json([
          'status' => 'success',
          'msg' => 'Published status has been updated.'
      ]);
    }

}
