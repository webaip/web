<?php

namespace App\Http\Controllers\CP\CompanyProfile;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Session;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\CompanyProfile as Model;


class CompanyProfileController extends Controller
{
    
    function __construct (){
       $this->route = "cp.company_profile";
    }

   
    public function index($page = ""){ 
        $data = Model::where('id', 1)->first();
        if(!empty($data)){
          return view('cp.company_profile.edit', ['route'=>$this->route,'data'=>$data]);
        }else{
          echo '404';die;
        }
    }

    public function showEditForm($id = 0){   
      $data = Model::where('id', $id)->first();
      if(!empty($data)){
        return view('cp.submit-form.edit', ['route'=>$this->route,'data'=>$data]);
      }else{
        return response(view('errors.404'), 404);
      }
    }

    public function update(Request $request){
      $id = $request->input('id');
      $image = "";
      Validator::make(
                  $request->all(), 
                  [
                      'file' => [
                                      'required',
                                      'mimes:jpeg,png,pdf,docx,doc'
                      ],
                  ], 

                  [
                    
                  ])->validate();
  
      $file = FileUpload::uploadFile($request, 'file', 'uploads/company_profile');
      if($file != ""){
          $data['file'] = $file; 
      }

      Model::where('id', $id)->update($data);
      Session::flash('msg', 'Data has been updated!' );
      return redirect()->back(); 
    }

   
   
}
