<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Session;
use Validator;


use App\Model\ContactAddress;
use App\Model\HumanResource;
use App\Model\WorkingHour;
use App\Model\Message;

use Telegram\Bot\Laravel\Facades\Telegram;
class ContactUsController extends FrontendController
{
    
    public function index() {
        $contact_address = ContactAddress::get();
        $human_resources = HumanResource::get();
        $working_hours   = WorkingHour::get();
        return view('frontend.contact-us', ['contact_address'=>$contact_address, 'human_resources'=>$human_resources,'working_hours'=>$working_hours ]);
    }

    public function put(Request $request){
        $data = array(
            'name' =>   $request->input('name'),
            'phone' =>  $request->input('phone'),
            'email' =>  $request->input('email'),
            'subject' =>  $request->input('subject'),
            'message' =>  $request->input('message') 
        );
        Session::flash('invalidData', $data );
        Validator::make(
            $request->all(), 
            [
                'name' => 'required|min:3|max:30',
                'subject' => 'required',
                'email' => 'email',
                'message' => 'required|max:255',
                //'g-recaptcha-response' => 'required',
            ], 

            [
                
            ])->validate();

        
        $id=Message::insertGetId($data);
        
        $text = "<b>Customer Contact </b>\n"
        . "<b>Name: </b> $request->name \n"
        . "<b>Phone: </b> $request->phone \n"
        . "<b>Email Address: </b> $request->email \n"
        . "<b>Subject: </b> $request->subject \n"
        . "<b>Message: </b> $request->message";

        Telegram::sendMessage([
            'chat_id' => env('TELEGRAM_CHANNEL_ID', ''),
            'text' => $text,
            'parse_mode' => 'HTML'
        ]);
        Session::flash('msg', 'Your request has been sent! We will respone you soon.' );
        return redirect(route('contact-us','#send-contact'));
    }
}
