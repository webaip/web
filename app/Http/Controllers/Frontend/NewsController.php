<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Model\News;

class NewsController extends FrontendController
{
    
    public function index($name) {
        if($name == 'atp-consultant'){
            $data = News::where(['service_id'=> 1,'is_published'=> 1 ])->orderBy('created_at', 'DESC')->paginate(12);
        }else if($name == 'atp-school-of-hospitality-and-tourism'){
            $data = News::where(['service_id'=> 2,'is_published'=> 1 ])->orderBy('created_at', 'DESC')->paginate(12);
        }else{
            $data = News::where(['service_id'=> 3,'is_published'=> 1 ])->orderBy('created_at', 'DESC')->paginate(12);
        }
        return view('frontend.news.news', ['data'=>$data]);
    }

    public function detail($name="", $slug = "") {
        if($name == 'atp-consultant'){
            $data = News::where(['service_id'=> 1,'slug'=> $slug ])->first();
        }else if($name == 'atp-school-of-hospitality-and-tourism'){
            $data = News::where(['service_id'=> 2,'slug'=> $slug ])->first();
        }else{
            $data = News::where(['service_id'=> 3,'slug'=> $slug ])->first();
        }
        return view('frontend.news.news-detail', ['data'=>$data]);

    }
}
