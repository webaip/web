<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Model\Project;

class ProjectController extends FrontendController
{
    
    public function index($name) {
        if($name == 'atp-consultant'){
            $data = Project::where(['service_id'=> 1,'is_published'=> 1 ])->orderBy('data_order', 'ASC')->paginate(12);
        }else if($name == 'atp-school-of-hospitality-and-tourism'){
            $data = Project::where(['service_id'=> 2,'is_published'=> 1 ])->orderBy('data_order', 'ASC')->paginate(12);
        }else{
            $data = Project::where(['service_id'=> 3,'is_published'=> 1 ])->orderBy('data_order', 'ASC')->paginate(12);
        }
        return view('frontend.project.projects', ['data'=>$data]);
    }

    public function detail($name="", $slug = "") {
        if($name == 'atp-consultant'){
            $data = Project::where(['service_id'=> 1,'slug'=> $slug ])->first();
        }else if($name == 'atp-school-of-hospitality-and-tourism'){
            $data = Project::where(['service_id'=> 2,'slug'=> $slug ])->first();
        }else{
            $data = Project::where(['service_id'=> 3,'slug'=> $slug ])->first();
        }
        return view('frontend.project.project-detail', ['data'=>$data]);

    }
}
