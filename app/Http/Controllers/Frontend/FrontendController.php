<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Session;
use Validator;

use App\Model\Consultation;
use Telegram\Bot\Laravel\Facades\Telegram;
class FrontendController extends Controller

{
   
   

	public $defaultData = array();
    public function __construct(){

      
    }

    public function defaultData($locale = "en"){


    	App::setLocale($locale);

        //Current Language
        $parameters = Route::getCurrentRoute()->parameters();

        return $this->defaultData;
    }

    public function submitConsultant(Request $request){
        $data = array(
            'name' =>   $request->input('name'),
            'email' =>  $request->input('email'),
            'services' =>  $request->input('services'),
            'message' =>  $request->input('message') 
        );
        Session::flash('invalidData', $data );
        Validator::make(
            $request->all(), 
            [
                'name' => 'required|min:3|max:30',
                'services' => 'required',
                'email' => 'email',
                'message' => 'required|max:255',
                // 'g-recaptcha-response' => 'required',
            ], 

            [
                
            ])->validate();

        
        $id=Consultation::insertGetId($data);

        $text = "<b>Get a Free Consultantion </b>\n"
        . "<b>Name: </b> $request->name \n"
        . "<b>services: </b> $request->services \n"
        . "<b>Email Address: </b> $request->email \n"
        . "<b>Message: </b> $request->message";

        Telegram::sendMessage([
            'chat_id' => env('TELEGRAM_CHANNEL_ID', ''),
            'text' => $text,
            'parse_mode' => 'HTML'
        ]);

        Session::flash('msg', __('contact_us.contact-successful-sent') );
        return redirect(route('home','#submit-consultant'));
    }
    

}
