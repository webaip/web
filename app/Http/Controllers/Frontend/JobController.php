<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Model\Job;

class JobController extends FrontendController
{
    
    public function index($name = '') {
        $today = date('Y-m-d'); 
        if($name == 'atp-consultant'){
            $data = Job::where(['service_id'=> 1,'is_published'=> 1 ])->where('closed_date', '>=', $today)->orderBy('data_order', 'ASC')->paginate(12);
        }else if($name == 'atp-school-of-hospitality-and-tourism'){
            $data = Job::where(['service_id'=> 2,'is_published'=> 1 ])->where('closed_date', '>=', $today)->orderBy('data_order', 'ASC')->paginate(12);
        }else{
            $data = Job::where(['service_id'=> 3,'is_published'=> 1 ])->where('closed_date', '>=', $today)->orderBy('data_order', 'ASC')->paginate(12);
        }
        return view('frontend.job.jobs', ['data'=>$data]);
    }

    public function detail($name = '', $slug = '') {
        $today = date('Y-m-d'); 
        if($name == 'atp-consultant'){
            $data = Job::where(['service_id'=> 1,'slug'=> $slug ])->where('closed_date', '>=', $today)->first();
        }else if($name == 'atp-school-of-hospitality-and-tourism'){
            $data = Job::where(['service_id'=> 2,'slug'=> $slug ])->where('closed_date', '>=', $today)->first();
        }else{
            $data = Job::where(['service_id'=> 3,'slug'=> $slug ])->where('closed_date', '>=', $today)->first();
        }
        return view('frontend.job.job-detail', ['data'=>$data]);
    }
}
