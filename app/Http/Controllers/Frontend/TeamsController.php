<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Team;

class TeamsController extends FrontendController
{
    
    public function index() {
        $teams    = Team::where('is_published', 1)->get();
        return view('frontend.teams',['teams' => $teams ]);
  
 }
}