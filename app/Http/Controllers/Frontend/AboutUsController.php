<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\AboutUs;
use App\Model\Team;

class AboutUsController extends FrontendController
{
    
    public function index() {
        $about_us = AboutUs::first();
        $teams    = Team::where('is_published', 1)->get();
        return view('frontend.about-us', ['about_us' => $about_us, 'teams' => $teams]);
    }
}
