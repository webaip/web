<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Model\OurBussiness;

class OurBusinessController extends FrontendController
{
    
    public function index($name) {
        if($name == 'atp-consultant'){
            $data = OurBussiness::where('service_id', 1)->first();
        }else if($name == 'atp-school-of-hospitality-and-tourism'){
            $data = OurBussiness::where('service_id', 2)->first();
        }else{
            $data = OurBussiness::where('service_id', 3)->first();
        }
        return view('frontend.our-bussinesses.'.$name, ['data' => $data]);
    }
}
