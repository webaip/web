<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Product;
use App\Model\Slide;
use App\Model\Logo;
use App\Model\Feedback;
use App\Model\Partner;
use App\Model\News;
use App\Model\Activity;
use App\Model\Project;
use App\Model\Information;
use App\Model\ConsultationContent;


class HomeController extends FrontendController
{
    
    public function index() {
       
        $slides     = Slide::where('is_published', 1)->orderBy('data_order', 'ASC')->get();
        $partners   = Partner::where('is_published', 1)->orderBy('data_order', 'ASC')->get();
        $feedbacks  = Feedback::where('is_published', 1)->orderBy('data_order', 'ASC')->get();
        $news       = News::where(['is_published' => 1, 'is_featured' => 1])->orderBy('data_order', 'ASC')->limit(9)->get();
        $projects   = Project::where(['is_published' => 1, 'is_featured' => 1])->orderBy('data_order', 'ASC')->limit(9)->get();
        $activities = Activity::where(['is_published' => 1])->orderBy('data_order', 'ASC')->limit(4)->get();
        $informations  = Information::orderBy('data_order', 'ASC')->get();

        $consultant_content = ConsultationContent::first();
        return view('frontend.home', [
                                    
                                    'slides'=>$slides, 
                                    'partners'=>$partners, 
                                    'feedbacks'=>$feedbacks,
                                    'news' => $news,
                                    'activities' => $activities,
                                    'projects'   => $projects,
                                    'informations' => $informations,
                                    'consultant_content' => $consultant_content,
                                    ]);
    }

    function x(){
    	$data = Product::select('*')->orderBy('price', 'DESC')->limit(10)->get();  
    	return view('frontend.x', ['data'=>$data]);
    }
}
