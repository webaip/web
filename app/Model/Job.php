<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
   
    protected $table = 'jobs';
   	
   	public function applys(){
        return $this->hasMany('App\Model\JobApply');
    }
}
