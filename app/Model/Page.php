<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
   
    protected $table = 'pages';

    public function banners(){
        return $this->belongsToMany('App\Model\Banner','page_banner');
    }
    public function pageBanners(){
        return $this->hasMany('App\Model\PageBanner');
    }
}
