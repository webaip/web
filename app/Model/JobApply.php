<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class JobApply extends Model
{
   
    protected $table = 'job_apply';
   	
   	public function job(){
        return $this->belongsTo('App\Model\Job');
    }
}
