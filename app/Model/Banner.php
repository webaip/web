<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
   
    protected $table = 'banner';
   
    public function pages(){
        return $this->belongsToMany('App\Model\Page','page_banner');
    }

    public function pageBanners(){
        return $this->hasMany('App\Model\PageBanner');
    }
}
