<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class PageBanner extends Model
{
   
    protected $table = 'page_banner';
    
    public function page(){
        return $this->belongsTo('App\Model\Page');
    }
    public function banner(){
        return $this->belongsTo('App\Model\Banner');
    }
}
