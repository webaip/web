<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class OurBussiness extends Model
{
   
    protected $table = 'our_bissinesses';
   
    public function service(){
        return $this->belongsTo('App\Model\Service');
    }
}
