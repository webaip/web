<?php

use Illuminate\Database\Seeder;

class TeamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('teams')->insert(
            [
                [
                    'name'       => 'Richard Antony',
                    'position'   => 'CEO / Founder',
                    'email'      => 'contact@atpconsultant.com',
                    'description' => 'Great explorer of the truth, the master-builder of human happiness no one rejects avoids.',
                    'image'      => 'public/frontend/images/team/1.jpg',
                    'is_published'   => 1,
                ],
                [
                    'name'       => 'Richard Antony',
                    'position'   => 'CEO / Founder',
                    'email'      => 'contact@atpconsultant.com',
                    'description' => 'Great explorer of the truth, the master-builder of human happiness no one rejects avoids.',
                    'image'      => 'public/frontend/images/team/2.jpg',
                    'is_published'   => 1,
                ],
                [
                    'name'       => 'Richard Antony',
                    'position'   => 'CEO / Founder',
                    'email'      => 'contact@atpconsultant.com',
                    'description' => 'Great explorer of the truth, the master-builder of human happiness no one rejects avoids.',
                    'image'      => 'public/frontend/images/team/3.jpg',
                    'is_published'   => 1,
                ],
                
            ]
        );
	}
}
