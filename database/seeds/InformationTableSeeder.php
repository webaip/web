<?php

use Illuminate\Database\Seeder;

class InformationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('informations')->insert(
            [
                [
                    'name'       => 'Years Of Experience',
                    'number'       => '5',
                ],
                [
                    'name'       => 'Training',
                    'number'       => '145',
                ],
                [
                    'name'       => 'Awards Winning',
                    'number'       => '92',
                ],
                [
                    'name'       => 'Happy Clients',
                    'number'       => '282',
                ],
            ]
        );
	}
}
