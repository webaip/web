<?php

use Illuminate\Database\Seeder;

class ProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('projects')->insert(
            [
                [
                    'service_id'          => '1',
                    'title'          => 'Business Growth',
                    'slug'           => 'business-growth',
                    'description'    => 'Great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself pleasure.',
                    'content'        => 'Great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself pleasure.',
                    'image'          => 'public/frontend/CamCyber/project/1.jpg',
                    'image_detail'   => 'public/frontend/images/services/service-single/single-service-1.jpg',
                    'is_published'   =>    1,
                    'is_featured'    =>    1,
                ],

                [
                    'service_id'          => '1',
                    'title'          => 'Sustainability',
                    'slug'           => 'sustainability',
                    'description'    => 'Great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself pleasure.',
                    'content'        => 'Great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself pleasure.',
                    'image'          => 'public/frontend/CamCyber/project/2.jpg',
                    'image_detail'   => 'public/frontend/images/services/service-single/single-service-1.jpg',
                    'is_published'   =>    1,
                    'is_featured'    =>    1,
                ],
                
                [
                    'service_id'          => '2',
                    'title'          => 'Performance',
                    'slug'           => 'performance',
                    'description'    => 'Great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself pleasure.',
                    'content'        => 'Great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself pleasure.',
                    'image'          => 'public/frontend/CamCyber/project/3.jpg',
                    'image_detail'   => 'public/frontend/images/services/service-single/single-service-1.jpg',
                    'is_published'   =>    1,
                    'is_featured'    =>    1,
                ],
                [
                    'service_id'          => '2',
                    'title'          => 'Customer Insights',
                    'slug'           => 'customer-insights',
                    'description'    => 'Great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself pleasure.',
                    'content'        => 'Great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself pleasure.',
                    'image'          => 'public/frontend/CamCyber/project/4.jpg',
                    'image_detail'   => 'public/frontend/images/services/service-single/single-service-1.jpg',
                    'is_published'   =>    1,
                    'is_featured'    =>    1,
                ],
                [
                    'service_id'          => '3',
                    'title'          => 'Performance',
                    'slug'           => 'performance',
                    'description'    => 'Great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself pleasure.',
                    'content'        => 'Great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself pleasure.',
                    'image'          => 'public/frontend/CamCyber/project/3.jpg',
                    'image_detail'   => 'public/frontend/images/services/service-single/single-service-1.jpg',
                    'is_published'   =>    1,
                    'is_featured'    =>    1,
                ],
                [
                    'service_id'          => '3',
                    'title'          => 'Customer Insights',
                    'slug'           => 'customer-insights',
                    'description'    => 'Great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself pleasure.',
                    'content'        => 'Great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself pleasure.',
                    'image'          => 'public/frontend/CamCyber/project/4.jpg',
                    'image_detail'   => 'public/frontend/images/services/service-single/single-service-1.jpg',
                    'is_published'   =>    1,
                    'is_featured'    =>    1,
                ],
            ]
        );
	}
}
