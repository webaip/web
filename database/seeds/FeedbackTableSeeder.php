<?php

use Illuminate\Database\Seeder;

class FeedbackTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('feedbacks')->insert(
            [
                [
                    'name'       => 'Nathan Duckvat',
                    'address'       => 'California',
                    'image'          => 'public/frontend/images/resources/quote-1.jpg',
                    'description'       => 'They have got my project on time with the competition with ahighly skilled, well-organized andexperienced team of professional Engineers well document.',
                    'is_published'   =>    1,
                ],
                [
                    'name'       => 'Bo Pisey',
                    'address'       => 'Phnom Penh',
                    'image'          => 'public/frontend/images/resources/quote-1.jpg',
                    'description'       => 'They have got my project on time with the competition with ahighly skilled, well-organized andexperienced team of professional Engineers well document.',
                    'is_published'   =>    1,
                ],
                [
                    'name'       => 'Leng Chinghor',
                    'address'       => 'Phnom Penh',
                    'image'          => 'public/frontend/images/resources/quote-1.jpg',
                    'description'       => 'They have got my project on time with the competition with ahighly skilled, well-organized andexperienced team of professional Engineers well document.',
                    'is_published'   =>    1,
                ],
            ]
        );
	}
}
