<?php

use Illuminate\Database\Seeder;

class PageBannerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('page_banner')->insert(
            [
                [
                    'page_id'       => 1,
                    'banner_id'       => 1,
                ],
                [
                    'page_id'       => 2,
                    'banner_id'       => 2,
                ],
                [
                    'page_id'       => 3,
                    'banner_id'       => 2,
                ],
                [
                    'page_id'       => 4,
                    'banner_id'       => 2,
                ],
                [
                    'page_id'       => 5,
                    'banner_id'       => 1,
                ],
                [
                    'page_id'       => 6,
                    'banner_id'       => 1,
                ],
                [
                    'page_id'       => 7,
                    'banner_id'       => 1,
                ],
                [
                    'page_id'       => 8,
                    'banner_id'       => 1,
                ],
                [
                    'page_id'       => 9,
                    'banner_id'       => 1,
                ],
                [
                    'page_id'       => 10,
                    'banner_id'       => 1,
                ],
                
            ]
        );
	}
}
