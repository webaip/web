<?php

use Illuminate\Database\Seeder;

class WorkingHourTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('working_hours')->insert(
            [
                [
                    'day'       => 'Monday',
                    'time'      => '07:00am - 05:30pm',
                ],
                [
                    'day'       => 'Tuesday',
                    'time'      => '07:00am - 05:30pm',
                ],
                [
                    'day'       => 'Wednesday',
                    'time'      => '07:00am - 05:30pm',
                ],
                [
                    'day'       => 'Thursday',
                    'time'      => '07:00am - 05:30pm',
                ],
                [
                    'day'       => 'Friday',
                    'time'      => '07:00am - 05:30pm',
                ],
                [
                    'day'       => 'Saturday',
                    'time'      => '07:00am - 05:30pm',
                ],
                [
                    'day'       => 'Sunday',
                    'time'      => 'closed',
                ],
            ]
        );
	}
}
