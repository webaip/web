<?php

use Illuminate\Database\Seeder;

class HumanResourceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('human_resources')->insert(
            [
                [
                    'name'           => 'Charles Mecky',
                    'image'          => 'public/frontend/images/resources/contact-1.jpg',
                    'phone'          => '+(855) 12 556 78',
                    'email'          => 'mecky@atp.com',
                ],
                [
                    'name'           => 'Robert Fertly',
                    'image'          => 'public/frontend/images/resources/contact-2.jpg',
                    'phone'          => '+(855) 12 556 78',
                    'email'          => 'mecky@atp.com',
                ],
                
            ]
        );
	}
}
