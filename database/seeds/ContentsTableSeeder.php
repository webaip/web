<?php

use Illuminate\Database\Seeder;

class ContentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('contents')->insert(
            [
                //============================= Footer
                [
                    'page'              => 'footer',
                    'slug'              => 'footer-content', 
                    'name'              => 'Footer Content',   
                    'content'           => 'Over 5 years experience & knowledge international standards, technologicaly changes & industrial systems, we are dedicated to provides seds the best & solutions to our valued customers.', 
                    'image_required'    => 0,
                    'image'             => '',
                    'width'             => 0,
                    'height'            => 0,
                    'editor_required'   => 1,
                    'updater_id'        => 1,
                ],

                [
                    'page'              => 'footer',
                    'slug'              => 'footer-adress', 
                    'name'              => 'Footer Adress',   
                    'content'           => 'Street 150, H#166, Sankat Tuek LaakII, Khan Toul Kork, Phnom Penh of Cambodia', 
                    'image_required'    => 0,
                    'image'             => '',
                    'width'             => 0,
                    'height'            => 0,
                    'editor_required'   => 1,
                    'updater_id'        => 1,
                ],

                [
                    'page'              => 'footer',
                    'slug'              => 'footer-phone', 
                    'name'              => 'Footer Phone',   
                    'content'           => '010 696 099 | 012 398 353', 
                    'image_required'    => 0,
                    'image'             => '',
                    'width'             => 0,
                    'height'            => 0,
                    'editor_required'   => 1,
                    'updater_id'        => 1,
                ],
                [
                    'page'              => 'footer',
                    'slug'              => 'footer-email', 
                    'name'              => 'Footer Email',   
                    'content'           => 'info@atpconsultant.com', 
                    'image_required'    => 0,
                    'image'             => '',
                    'width'             => 0,
                    'height'            => 0,
                    'editor_required'   => 1,
                    'updater_id'        => 1,
                ],
                //============================= Hompage
                [
                    'page'              => 'homepage',
                    'slug'              => 'professional-reliably-content', 
                    'name'              => 'Professional & Reliably Content',   
                    'content'           => '<p>Explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete.</p>', 
                    'image_required'    => 0,
                    'image'             => '',
                    'width'             => 0,
                    'height'            => 0,
                    'editor_required'   => 1,
                    'updater_id'        => 1,
                ],
                [
                    'page'              => 'homepage',
                    'slug'              => 'experience-content', 
                    'name'              => 'Experience Content',   
                    'content'           => '<p>Explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete.</p>', 
                    'image_required'    => 0,
                    'image'             => '',
                    'width'             => 0,
                    'height'            => 0,
                    'editor_required'   => 1,
                    'updater_id'        => 1,
                ],

                [
                    'page'              => 'homepage',
                    'slug'              => 'solutions', 
                    'name'              => 'Solution Content',   
                    'content'           => 'There is some reason behind why people choose ATP past 5 years for their business solutions.', 
                    'image_required'    => 0,
                    'image'             => '',
                    'width'             => 0,
                    'height'            => 0,
                    'editor_required'   => 1,
                    'updater_id'        => 1,
                ],

                [
                    'page'              => 'homepage',
                    'slug'              => 'consumer-products', 
                    'name'              => 'Consumer Products',   
                    'content'           => 'Must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and we will give you a complete account of the system expound.', 
                    'image_required'    => 0,
                    'image'             => '',
                    'width'             => 0,
                    'height'            => 0,
                    'editor_required'   => 1,
                    'updater_id'        => 1,
                ],
               
            ]
        );
	}
}
