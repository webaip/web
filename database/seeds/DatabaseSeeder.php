<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //User
        $this->call(ServiceTableSeeder::class);
        $this->call(PositionsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ContentsTableSeeder::class);
        $this->call(ActivityTableSeeder::class);
        $this->call(FeedbackTableSeeder::class);
        $this->call(InformationTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(PageTableSeeder::class);
        $this->call(PartnerTableSeeder::class);
        $this->call(SlideTableSeeder::class);
        $this->call(ProjectTableSeeder::class);
        $this->call(ConsultationContentTableSeeder::class);
        $this->call(AboutUsTableSeeder::class);
        $this->call(TeamTableSeeder::class);
        $this->call(OurBissinessesTableSeeder::class);
        $this->call(JobTableSeeder::class);
        $this->call(ContactAddressTableSeeder::class);
        $this->call(HumanResourceTableSeeder::class);
        $this->call(WorkingHourTableSeeder::class);
        $this->call(BannerTableSeeder::class);
        $this->call(PageBannerTableSeeder::class);
        $this->call(CompanyProfileTableSeeder::class);
        $this->call(LogoTableSeeder::class);
        
    }
}
