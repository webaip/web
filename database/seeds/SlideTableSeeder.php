<?php

use Illuminate\Database\Seeder;

class SlideTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('slides')->insert(
            [
                [
                    'title'       => 'ATP Consultant Co., Ltd',
                    'image'          => 'public/frontend/CamCyber/slide/1.jpg',
                    'is_published'   =>    1,
                ],
                [
                    'title'       => 'ATP School of Hospitality and Tourism',
                    'image'          => 'public/frontend/CamCyber/slide/2.jpg',
                    'is_published'   =>    1,
                ],
                [
                    'title'       => 'Green Café and Restaurant',
                    'image'          => 'public/frontend/CamCyber/slide/3.jpg',
                    'is_published'   =>    1,
                ],
                
            ]
        );
	}
}
