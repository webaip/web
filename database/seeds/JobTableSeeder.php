<?php

use Illuminate\Database\Seeder;

class JobTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('jobs')->insert(
            [
                [
                    'service_id'       => '1',
                    'position'       => 'Web Developer',
                    'slug'        => 'web-developer',
                    'location'        => 'Phnom Penh',
                    'image'          => 'public/frontend/images/blog/blog-large-1.jpg',
                    'description'       => 'Pursues or desires to obtain pain of itself our because it is pain, but because occasionally can procure great pleasure.',
                    'requirment'       => 'Pursues or desires to obtain pain of itself our because it is pain, but because occasionally can procure great pleasure.',
                    'is_published'   =>    1,
                    'start_date'   =>    '2019-02-18',
                    'closed_date'   =>    '2019-03-30',
                ],
                [
                    'service_id'       => '1',
                    'position'       => 'Web Developer',
                    'slug'        => 'web-developer',
                    'location'        => 'Phnom Penh',
                    'image'          => 'public/frontend/images/blog/blog-large-1.jpg',
                    'description'       => 'Pursues or desires to obtain pain of itself our because it is pain, but because occasionally can procure great pleasure.',
                    'requirment'       => 'Pursues or desires to obtain pain of itself our because it is pain, but because occasionally can procure great pleasure.',
                    'is_published'   =>    1,
                    'start_date'   =>    '2019-02-18',
                    'closed_date'   =>    '2019-03-30',
                ],
                [
                    'service_id'       => '2',
                    'position'       => 'Web Developer',
                    'slug'        => 'web-developer',
                    'location'        => 'Phnom Penh',
                    'image'          => 'public/frontend/images/blog/blog-large-1.jpg',
                    'description'       => 'Pursues or desires to obtain pain of itself our because it is pain, but because occasionally can procure great pleasure.',
                    'requirment'       => 'Pursues or desires to obtain pain of itself our because it is pain, but because occasionally can procure great pleasure.',
                    'is_published'   =>    1,
                    'start_date'   =>    '2019-02-18',
                    'closed_date'   =>    '2019-03-30',
                ],
                [
                    'service_id'       => '3',
                    'position'       => 'Web Developer',
                    'slug'        => 'web-developer',
                    'location'        => 'Phnom Penh',
                    'image'          => 'public/frontend/images/blog/blog-large-1.jpg',
                    'description'       => 'Pursues or desires to obtain pain of itself our because it is pain, but because occasionally can procure great pleasure.',
                    'requirment'       => 'Pursues or desires to obtain pain of itself our because it is pain, but because occasionally can procure great pleasure.',
                    'is_published'   =>    1,
                    'start_date'   =>    '2019-02-18',
                    'closed_date'   =>    '2019-03-30',
                ],
            ]
        );
	}
}
