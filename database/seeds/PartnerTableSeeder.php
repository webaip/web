<?php

use Illuminate\Database\Seeder;

class PartnerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('partners')->insert(
            [
                [
                    'title'       => 'Ministry of Tourism',
                    'image'          => 'public/frontend/CamCyber/client/1.png',
                    'url'         => '#',  
                    'is_published'   =>    1,
                ],
                [
                    'title'       => 'PSE',
                    'image'          => 'public/frontend/CamCyber/client/2.png',
                    'url'         => '#', 
                    'is_published'   =>    1,
                ],
                [
                    'title'       => 'SDP',
                    'image'          => 'public/frontend/CamCyber/client/3.png',
                    'url'         => '#', 
                    'is_published'   =>    1,
                ],
                [
                    'title'       => 'Coma',
                    'image'          => 'public/frontend/CamCyber/client/4.png',
                    'url'         => '#', 
                    'is_published'   =>   1,
                ],
                [
                    'title'       => 'Tonle',
                    'image'          => 'public/frontend/CamCyber/client/5.png',
                    'url'         => '#', 
                    'is_published'   =>   1,
                ],
                [
                    'title'       => 'ELITE',
                    'image'          => 'public/frontend/CamCyber/client/6.png',
                    'url'         => '#', 
                    'is_published'   =>   1,
                ],
                [
                    'title'       => 'SmartaFood',
                    'image'          => 'public/frontend/CamCyber/client/7.png',
                    'url'         => '#', 
                    'is_published'   =>   1,
                ],
                [
                    'title'       => 'Nagaword',
                    'image'          => 'public/frontend/CamCyber/client/8.png',
                    'url'         => '#', 
                    'is_published'   =>   1,
                ],
                [
                    'title'       => 'ID',
                    'image'          => 'public/frontend/CamCyber/client/9.png',
                    'url'         => '#', 
                    'is_published'   =>   1,
                ],
                [
                    'title'       => 'Green Left',
                    'image'          => 'public/frontend/CamCyber/client/10.png',
                    'url'         => '#', 
                    'is_published'   =>   1,
                ],
                [
                    'title'       => 'APT Consultant',
                    'image'          => 'public/frontend/CamCyber/client/11.png',
                    'url'         => '#', 
                    'is_published'   =>   1,
                ],
            ]
        );
	}
}
