<?php

use Illuminate\Database\Seeder;

class PageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('pages')->insert(
            [
                [
                    'name'       => 'About Us',
                ],
                [
                    'name'       => 'ASEAN Tourism Professional Consultant',
                ],
                [
                    'name'       => 'School Of Hospitality And Tourism',
                ],
                [
                    'name'       => 'Green Cafe And Restaurant',
                ],
                [
                    'name'       => 'News',
                ],
                [
                    'name'       => 'News Detail',
                ],
                [
                    'name'       => 'Project',
                ],
                [
                    'name'       => 'Project Detail',
                ],
                [
                    'name'       => 'Job Opportunities',
                ],
                
                [
                    'name'       => 'Contact Us',
                ],
            ]
        );
	}
}
