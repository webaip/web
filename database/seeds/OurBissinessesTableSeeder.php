<?php

use Illuminate\Database\Seeder;

class OurBissinessesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('our_bissinesses')->insert(
            [
                
                [
                    'service_id'    => '1',
                    'banner'        => 'public/frontend/CamCyber/consultant.jpg',
                    'content'       => '
                    <div class="text-holder">
                                    <h3>About Us</h3>
                                    <p class="mar-btm15">ATP Consultant Co., Ltd was established in 2015 by a number of outstanding Cambodian ASEAN master trainers and assessors who acquire both experiences and qualification in the field of hospitality training. ATP Consultant is the abbreviation of ASEAN Tourism Professionals Consultant. ATP Consultant is strongly recognized by the Cambodia Ministry of Tourism, National Committee for Tourism Professional and ASEAN Member States for its outstanding work within Mutual Recognition Arrangement on Tourism Professionals (MRA-TP) and ASEAN Qualification Reference Framework (AQRF).</p>

                                    <p class="mar-btm15"> Each Board of Director of the ATP Consultant has more than 10 years of experience in the tourism and hospitality industry, and more than five year experiences in implementing the ASEAN Common Competency Standard for Tourism Professionals (ACCSTP) under the ASEAN MRA-TP framework. ATP Consultant team has been working closely with the NCTP to provide the National Master Trainers and Master Assessors training in housekeeping, food and beverage services, and front office division. The team also facilitated the pilot project of Recognition of Prior Learning Assessment for PSE in 2015. </p>

                                    <p class="mar-btm15"> ATP Consultant commits to provide high quality products and services which align with vision and strategic action of the Cambodia Ministry of Tourism and the National Committee for Tourism Professionals in order to promote the implementation of ASEAN MRA-TP for the successful of human resource development in tourism and hospitality sector.  
									 </p>
                                    
                                    <h3>Our Vision</h3>
                                    <p class="mar-btm15">
                                        Our vision is to become a leader in skill training, assessment and consulting on the basis of the ASEAN Mutual Recognition Arrangement on Tourism Professionals (MRA-TP) for hospitality and tourism related businesses.
                                    </p>

                                    <h3>Mission</h3>
                                    <ul>
                                        <li> - To develop human resources in tourism and hospitality industry focusing on hotel and travel service on the basis of ASEAN Mutual Recognition Arrangement on Tourism Professionals. </li>
                                        <li> - To provide professional consulting with ethic and quality of services</li>
                                    </ul>


                                     <h3>Objective</h3>
                                    <p class="mar-btm15">
                                       </p><p>
                                           </p><ul>
                                               <li> - To promote human resource development in tourism and hospitality sector on the basis of   ASEAN Mutual Recognition Arrangement on Tourism Professionals.</li>
                                               <li> - To work with the Cambodia Ministry of Tourism to achieve its strategic plan in human resource development in tourism and hospitality industry.</li>
                                               <li> - To provide assistance to our partners to obtain qualification, skill passport and certificate of competence which are recognised by National Committee for Tourism Professionals.  </li>
                                           </ul>
                                       <p></p>

                                       <br>
                                      
                                    <p></p>
                                </div>

                    <div class="text-holder">
                                    <h3>Products &amp; Service</h3>
                                    <p class="mar-btm15">ATP Consultant provides three main products and service as following: </p>
                                    <h5><b>A- Consulting service</b></h5>
                                    <p class="mar-btm15">
                                    	We provide a variety of consulting services  ranging from developing business concept to training programme and  standard of operation procedure for both hospitality school and businesses: 
                                    	</p><ol>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Develop business concept for hospitality and tourism schools, hotels, restaurants, café shops and catering operations. 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Develop training programme for hospitality and tourism schools. 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Develop curriculums for hotel and travel service 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Develop staff capacity building program 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Develop standard of operation procedure (SOP) 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Training Needs Analysis (TNA)
                                    		</li>
                                    	</ol>
                                    <p></p>
                                    <h5><b>C- Assessment service</b></h5>
                                    <p class="mar-btm15">
                                    	</p><ol>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Competency – Based Assessment (CBA) under MRA-TP Framework
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> In-house training for tourism and hospitality related businesses  
                                    		</li>
                                    		
                                    	</ol>
                                    <p></p>
                                     <h5><b>B- Training service</b></h5>
                                    <p class="mar-btm15">
                                    	</p><ol>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Competency - Based Training (CBT) in hotel and travel service using the Common ASEAN Tourism Curriulum (CATC)
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Facilitate recognition of skill passport 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Conduct and facilitate the Recogniton of Prior Leaning (RPL) and the Recognition of Current Competencies (RCC) 
                                    		</li>
                                    		
                                    	</ol>
                                    <p></p>
                                </div>

                    <div class="text-holder">
                                    <h3>Porfolio</h3>
                                    <h5><b>Skill Development Program (SDP) – SwissContact Cambodia [yyyy – yyyy] </b></h5>
                                    <p class="mar-btm15">
                                    	</p><ol>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Develop curricula in the divisions of F&amp;B service, housekeeping, and front office for both low-skill workers and owners/managers 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Develop in-house training material for National Trainer and National Assessor in the divisions of F&amp;B service, housekeeping, and front office
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Develop Recognition of Prior Learning (RPL) mechanism and RPL manual for the National Committee for Tourism Professionals (NCTP) and SDP. 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Conduct skill test assessment for in-house training programme within 3 provinces of Kratie, Stung Treng and Preah Vihear 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Deliver training to hospitality enterprise owners/managers and officers of the provincial department of tourism 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Deliver training to low-skill workers on F&amp;B Service, front office and housekeeping
                                    		</li>
                                    	</ol>
                                    <p></p>

                                    <h5><b>Mekong Inclusive Growth and Innovation Programme (MIGIP) – SwissContact Cambodia [yyyy – yyyy]  </b></h5>
                                    <p class="mar-btm15">
                                    	</p><ol>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Develop training materials for National trainer in F&amp;B service Division
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Conduct Train of Trainer (ToT) training and assessment for national trainers 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Conduct an assessment study with owners/managers of hospitality enterprises in Kampot 
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Provide coaching to national trainers to deliver effective training
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Conduct Skill test Assessment to 40 candidate at Kampot based MIGIP projection and issue certificate of completion
                                    		</li>
                                    	</ol>
                                    <p></p>

                                    <h5><b>Barista RPL Assessment – National Committee for Tourism Professionals  [yyyy – yyyy]   </b></h5>
                                    <p class="mar-btm15">
                                    	</p><ol>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Provide consultation service on curricula and assessment material development
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Conduct RPL assessment to baristas from various café in Phnom Penh area.
                                    		</li>
                                    		
                                    	</ol>
                                    <p></p>

                                    <h5><b>Establishing Tourism Trainer Club (TTC)   </b></h5>
                                    <p class="mar-btm15">
                                    	</p><ol>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Gathering tourism professionals from various enterprises
                                    		</li>
                                    		<li>
                                    			<i class="fa fa-angle-right" aria-hidden="true"></i> Facilitate weekly training for TTC on the basis of ASEAN Tourism Reference Qualification Framework
                                    		</li>
                                    		
                                    	</ol>
                                    <p></p>

                                </div>
                    ',
                ],
                [
                    'service_id'       => '2',
                    'banner' => 'public/frontend/images/our-bussiness/school.jpg',
                    'content' => '
                    <div class="text-holder">
                    <h3>About Us</h3>
                    <p class="mar-btm15">We are proud to be amongst the first technical and vocational education and training schools in Cambodia to launch the ASEAN Mutual Recognition Arrangement for Tourism Professionals study programme. ATP School of Hospitality and Tourism (ATP-SoHT) was established in November 2017 by a number of tourism and hospitality professionals who acquire years of experience in the industry. Our aim is to provide the regionally recognized hospitality training programme that accommodates the needs of tourism job market in Cambodia and in ASEAN region. Our curricula are developed on the basis of the Common ASEAN Tourism Curriculum (CATC) which is the integral part of the ASEAN Mutual Recognition Arrangement on Tourism Professionals (MRA-TP). In March 2018 our curricula were endorsed by the National Committee for Tourism Professionals of the Ministry of Tourism.</p>
                    <p class="mar-btm15">

                       ATP-SoHT equips with all necessary training material and equipment that help students build their competencies including skill, knowledge and attitude to be tourism professionals. In this regard, ATP-SoHT own three classrooms, an application Green Café and Restaurant, a reception desk, a library and computer lab, a laundry room and a sample hotel room. Moreover, ATP-SoHT builds a strong network with various hospitality businesses, schools, NGO and governmental agencies to expand internship and job opportunities for our students.  
                        
                    </p>
                    
                    <h3>Our Vision</h3>
                    <p class="mar-btm15">
                        Our vision is to become the school of excellence in delivering quality of human resources for the fast growing hospitality and tourism industry in Cambodia. 
                    </p>

                    <h3>Mission</h3>
                    <ul>
                        <li> - To prepare students for challenging careers in the hospitality and tourism sectors.  </li>
                        <li> - To provide a hospitality training programme that addresses the needs national and regional hospitality and tourism industry.</li>
                    </ul>
                    <br>
                    <h3>Objective</h3>
                    <p class="mar-btm15">
                       </p><p>
                           </p><ul>
                               <li> - Develop high quality professionals for tourism and hospitality industry in Cambodia</li>
                               <li> - Provide hospitality and tourism education and training programme that aligns with national and regional standards.</li>
                               <li> - Provide a training programme that addresses the ever changing industry trends. </li>
                               <li> - Offer equal opportunities and create an equitable work environment for all youth living in different parts of the country, while ensuring sustainable growth in the hospitality and tourism industry.</li>
                           </ul>
                       <p></p>

                    <p></p>
                    <br>
                    <div class="text-holder">
                        <h3>Study Programme and Qualification</h3>
                        <br>
                        <h5><b>Higher Diploma in Hotel and Tourism Management  </b></h5>
                        <p>
                            The higher diploma in hotel and tourism management provided by ATP-SoHT is a qualification based vocational education programme which equals to level 5 in ASEAN Qualification Reference Framework and Associate Degree in Cambodian Higher Education.    It is a two year vocational education programme combining theoretical knowledge and practical skills.
                        </p>
                        <p class="mar-btm15">
                         </p><table class="table table-bordered">
                            <thead>
                              <tr>
                                <th>Year</th>
                                <th>Course</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Foundation</td>
                                <td>
                                    <p>
                                        General Tourism and Hospitality Knowledge 
                                    </p>
                                    <p>
                                        Understanding and Providing Food and Beverage (F&amp;B) Service 
                                    </p>
                                    <p>
                                        Understanding and Providing Front Office (FO) Service 
                                    </p>
                                    <p>
                                        Understanding and Providing Housekeeping Service  (HK)
                                    </p>
                                    <p>
                                        3 month Internship (Compulsory)
                                    </p>
                                </td>
                              </tr>
                              <tr>
                                <td>Year 2</td>
                                <td>
                                    <p>
                                        Principle of Hotel and Tourism Management
                                    </p>
                                    <p>
                                        Management in F&amp;B, FO and HK
                                    </p>
                                    <p>
                                        6 month internship (Compulsory)
                                    </p>
                                </td>
                              </tr>
                              
                            </tbody>
                        </table>
                        <h5><i>Vocational Certificate [to be developed]   </i></h5>
                            <ol>
                                <li>
                                    Year 1: 540 (per year) or 270 (per semester)
                                </li>
                                <li>
                                    Year 2: 560 (per year) or 280 (per semester) 
                                </li>
                            </ol>

                        <h5><b>Job Title Skill Training – Skilling Certificate [ to be developed]   </b></h5>
                           <p>
                               Competency Based Training (CBT) aligns with Cambodia Common Competency Standard on Tourism Professional (CCCSTP) for Hotel Service and Travel Services.
                           </p>
                        <h5><b>School Fees  </b></h5>

                        <br>
                        <h3>Resources </h3>
                        <ul>
                            <li> - Three classrooms [description]  </li>
                            <li> - A two storey application restaurant and café with a commercial kitchen [description] </li>
                            <li> - A sample hotel room [description]</li>
                            <li> - A laundry room [description]</li>
                            <li> - A computer lab and a library [description]</li>
                            <li> - A front desk [description]</li>
                        </ul>
                    </div>
                </div>
                <div class="text-holder">
                <h3>Study Programme and Qualification</h3>
                <br>
                <h5><b>Higher Diploma in Hotel and Tourism Management  </b></h5>
                <p>
                    The higher diploma in hotel and tourism management provided by ATP-SoHT is a qualification based vocational education programme which equals to level 5 in ASEAN Qualification Reference Framework and Associate Degree in Cambodian Higher Education.    It is a two year vocational education programme combining theoretical knowledge and practical skills.
                </p>
                <p class="mar-btm15">
                 </p><table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Year</th>
                        <th>Course</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Foundation</td>
                        <td>
                            <p>
                                General Tourism and Hospitality Knowledge 
                            </p>
                            <p>
                                Understanding and Providing Food and Beverage (F&amp;B) Service 
                            </p>
                            <p>
                                Understanding and Providing Front Office (FO) Service 
                            </p>
                            <p>
                                Understanding and Providing Housekeeping Service  (HK)
                            </p>
                            <p>
                                3 month Internship (Compulsory)
                            </p>
                        </td>
                      </tr>
                      <tr>
                        <td>Year 2</td>
                        <td>
                            <p>
                                Principle of Hotel and Tourism Management
                            </p>
                            <p>
                                Management in F&amp;B, FO and HK
                            </p>
                            <p>
                                6 month internship (Compulsory)
                            </p>
                        </td>
                      </tr>
                      
                    </tbody>
                </table>
                <h5><i>Vocational Certificate [to be developed]   </i></h5>
                    <ol>
                        <li>
                            Year 1: 540 (per year) or 270 (per semester)
                        </li>
                        <li>
                            Year 2: 560 (per year) or 280 (per semester) 
                        </li>
                    </ol>

                <h5><b>Job Title Skill Training – Skilling Certificate [ to be developed]   </b></h5>
                   <p>
                       Competency Based Training (CBT) aligns with Cambodia Common Competency Standard on Tourism Professional (CCCSTP) for Hotel Service and Travel Services.
                   </p>
                <h5><b>School Fees  </b></h5>

                <br>
                <h3>Resources </h3>
                <ul>
                    <li> - Three classrooms [description]  </li>
                    <li> - A two storey application restaurant and café with a commercial kitchen [description] </li>
                    <li> - A sample hotel room [description]</li>
                    <li> - A laundry room [description]</li>
                    <li> - A computer lab and a library [description]</li>
                    <li> - A front desk [description]</li>
                </ul>
            </div>
                    
                    ',
                ],
                [
                    'service_id'       => '3',
                    'banner'           =>'public/frontend/images/our-bussiness/resautrant.jpg',
                    'content'          => '
                    <div class="text-holder">
                    <h3>About Us</h3>
                    <p class="mar-btm15">Green is an application Café and restaurant of the ATP School of Hospitality and Tourism. Green Café and Restaurant serves as a learning ambiance for ATP_School of Hospitality and Tourism where students can acquire real-to-life experience in food and beverage service and production. The Café and restaurant provides a wide range of products focusing on healthy diets to general customers.  Moreover, we also provide catering service for small functions like birthday party, staff party and anniversary. Our cafe and restaurant operates from 6.30 am to 8.30 pm daily.  Our business slogan is Green Café and Restaurant serves your favorite healthy food and drink.</p>
                    
                    <h3>Our Vision</h3>
                    <p class="mar-btm15">
                        The franchise casual fine dining café and restaurant in Cambodia under TM of “Green Café &amp; Restaurant” to young investors throughout the country.
                    </p>

                    <h3>Mission</h3>
                    <!-- <p class="mar-btm15">
                        OUR MISSION is to Develop Human Resources in Hotel and Travel Services in Hospitality and Tourism Industries based on ASEAN Mutual Recognition Arrangement for Tourism Professionals Qualification Reference Framework by providing Skills, Knowledge and right Attitude to our partners. And To provide professionals consulting with ethic and quality services base on National Qualification and ASEAN Mutual Recognition Arrangement for Tourism Professionals Qualification Framework.

                    </p> -->
                    <ol>
                        <li>Builds capacity of young Cambodians to be able to manage and run the f&amp;b business </li>
                        <li>Provides healthy choices of food and drink to customers</li>
                    </ol>

                     <h3>Objective</h3>
                        <div style="    padding: 5px 0px 0px 0px;" class="row">
                            <div class="col-lg-4 col-md-4">
                               <img src="http://localhost/CamCyber/atp/web/public/frontend/images/our-bussiness/resautrant-obj.jpg">
                            </div>
                            <div class="col-lg-8 col-md-8">
                               <p class="mar-btm15">
                                    </p><p>
                                       </p><ul>
                                           <li> - Trains young Cambodians the knowledge, skills and attitude in the hospitality industry</li>
                                           <li> - Provides a first-hand working experience to Cambodian youth to manage the operation of a café and restaurant.</li>
                                           <li> - Offers a wide range of healthy food and drink options in an affordable price. </li>
                                           <li> - Offers MSG free food to customers</li>
                                           <li> - Maximize the use of local produced and fresh products and food and beverage preparation. </li>
                                       </ul>
                                    <p></p>
                                   <br>
                                <p></p> 
                            </div>
                        </div>
                        
                </div>

                <div style="    padding: 5px 0px 0px 0px;" class="row">
                    <div class="col-lg-4 col-md-4">
                        <img src="http://localhost/CamCyber/atp/web/public/frontend/images/our-bussiness/resautrant-obj.jpg">
                    </div>
                    <div class="col-lg-8 col-md-8">
                        <p class="mar-btm15">
                            </p><p>
                                </p><ul>
                                    <li> - Trains young Cambodians the knowledge, skills and attitude in the hospitality industry</li>
                                    <li> - Provides a first-hand working experience to Cambodian youth to manage the operation of a café and restaurant.</li>
                                    <li> - Offers a wide range of healthy food and drink options in an affordable price. </li>
                                    <li> - Offers MSG free food to customers</li>
                                    <li> - Maximize the use of local produced and fresh products and food and beverage preparation. </li>
                                </ul>
                            <p></p>
                            <br>
                        <p></p> 
                    </div>
                </div>
                    ',
                ],
                
            ]
        );
	}
}
