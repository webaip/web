<?php

use Illuminate\Database\Seeder;

class ServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('services')->insert(
            [
                
                [
                    'title'       => 'ASEAN Tourism Professional Consultant',
                ],
                [
                    'title'       => 'School Of Hospitality And Tourism',
                ],
                [
                    'title'       => 'Green Cafe And Restaurant',
                ],
                
            ]
        );
	}
}
