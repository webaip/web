<?php

use Illuminate\Database\Seeder;

class ContactAddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('contact_address')->insert(
            [
                [
                    'name'       => 'Asean Tourism Professional Consultant',
                    'address'    => ' Street 150, H#166, Sankat Tuek LaakII, Khan Toul Kork,Phnom Penh of Cambodia',
                    'phone'      => '010 696 099 | 012 398 353',
                    'email'      =>  'info@atpconsultant.com',
                ],
                [
                    'name'       => 'School of Hospitality and Tourism',
                    'address'    => ' Street 150, H#166, Sankat Tuek LaakII, Khan Toul Kork,Phnom Penh of Cambodia',
                    'phone'      => '010 696 099 | 012 398 353',
                    'email'      =>  'info@atpconsultant.com',
                ],
                [
                    'name'       => 'Green Cafe and Restaurant',
                    'address'    => ' Street 150, H#166, Sankat Tuek LaakII, Khan Toul Kork,Phnom Penh of Cambodia',
                    'phone'      => '010 696 099 | 012 398 353',
                    'email'      =>  'info@atpconsultant.com',
                ],
            ]
        );
	}
}
