<?php

use Illuminate\Database\Seeder;

class ActivityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('activities')->insert(
            [
                [
                    'title'       => 'Activity 1',
                    'image'          => 'public/frontend/CamCyber/activitiy/1.jpg',
                    'is_published'   =>    1,
                    'is_featured'   =>    1,
                ],
                [
                    'title'       => 'Activity 2',
                    'image'          => 'public/frontend/CamCyber/activitiy/2.jpg',
                    'is_published'   =>    1,
                    'is_featured'   =>    1,
                ],
                [
                    'title'       => 'Activity 3',
                    'image'          => 'public/frontend/CamCyber/activitiy/3.jpg',
                    'is_published'   =>    1,
                    'is_featured'   =>    1,
                ],
                [
                    'title'       => 'Activity 4',
                    'image'          => 'public/frontend/CamCyber/activitiy/4.jpg',
                    'is_published'   =>    1,
                    'is_featured'   =>    1,
                ],
            ]
        );
	}
}
