<?php

use Illuminate\Database\Seeder;

class BannerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('banner')->insert(
            [
                [
                    'title'       => 'About',
                    'image'          => 'public/frontend/CamCyber/bg.jpg',
                    'is_published'   =>    1,
                ],
                [
                    'title'       => 'Our businesses',
                    'image'          => 'public/frontend/CamCyber/banner-consultant.jpg',
                    'is_published'   =>    1,
                ],
                [
                    'title'       => 'Banners',
                    'image'          => 'public/frontend/images/resources/breadcrumb-bg.jpg',
                    'is_published'   =>    1,
                ],
                
            ]
        );
	}
}
