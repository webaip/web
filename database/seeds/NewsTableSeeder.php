<?php

use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('news')->insert(
            [
                [
                    'service_id'   => '1',
                    'title'       => 'A digital prescription for the pharma industry',
                    'slug'        => 'digital-prescription-for-the-pharma-industry',
                    'feature_image'          => 'public/frontend/images/blog/lat-blog-1.jpg',
                    'image'          => 'public/frontend/images/services/service-single/single-service-1.jpg',
                    'description'       => 'Pursues or desires to obtain pain of itself our because it is pain, but because occasionally can procure great pleasure.',
                    'is_published'   =>    1,
                    'is_featured'   =>    1,
                ],
                [
                    'service_id'   => '2',
                    'title'       => 'Why buying a big house is a bad investment',
                    'slug'        => 'why-buying-a-big-house-is-a-bad-investment',
                    'feature_image'          => 'public/frontend/images/blog/lat-blog-2.jpg',
                    'image'                  => 'public/frontend/images/services/service-single/single-service-1.jpg',
                    'description'       => 'Pursues or desires to obtain pain of itself our because it is pain, but because occasionally can procure great pleasure.',
                    'is_published'   =>    1,
                    'is_featured'   =>    1,
                ],
                [
                    'service_id'   => '3',
                    'title'       => 'Successfull business tips in 2017: way to grow',
                    'slug'        => 'successfull-business-tips-in-2017-way-to-grow',
                    'feature_image'          => 'public/frontend/images/blog/lat-blog-3.jpg',
                    'image'          => 'public/frontend/images/services/service-single/single-service-1.jpg',
                    'description'       => 'Pursues or desires to obtain pain of itself our because it is pain, but because occasionally can procure great pleasure.',
                    'is_published'   =>    1,
                    'is_featured'   =>    1,
                ],
            ]
        );
	}
}
