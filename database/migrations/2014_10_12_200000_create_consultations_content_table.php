<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultationsContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultations_content', function (Blueprint $table) {
            $table->increments('id', 11);
			$table->string('name', 250)->nullable();
            $table->string('num_of_experince', 250)->nullable();
            $table->string('experince', 250)->nullable();
            $table->text('description')->nullable();
           //The field that will appear for almost tables
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultations_content');
    }
}
