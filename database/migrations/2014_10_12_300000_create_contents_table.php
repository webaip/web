<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('page', 250);
            $table->string('slug', 250);
            $table->string('name', 250);
            $table->text('content');
            $table->string('note', 100)->default('');
            $table->boolean('image_required')->default(0);
            $table->string('image', 50)->default('');
            $table->integer('width')->default(0)->unsigned()->index()->nullable();
            $table->integer('height')->default(0)->unsigned()->index()->nullable();
            $table->boolean('editor_required')->default(0);
            $table->integer('updater_id')->default(1)->unsigned()->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
