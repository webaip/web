<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutUsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_us', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('data_order')->index()->nullable();
            $table->string('title', 550)->nullable();
            $table->text('description')->nullable();
            $table->string('ceo_name', 550)->nullable();
            $table->string('ceo_position', 550)->nullable();
            $table->string('image', 250)->nullable();
            $table->text('mission')->nullable();
            $table->text('vision')->nullable();
            $table->text('value')->nullable();
           //The field that will appear for almost tables
            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_us');
    }
}
