<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobApplyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_apply', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('job_id')->unsigned()->index()->nullable();
            $table->foreign('job_id')->references('id')->on('jobs');
            $table->string('name', 550)->nullable();
            $table->string('phone', 550)->nullable();
            $table->string('email', 550)->nullable();
            $table->string('cv', 550)->nullable();
           //The field that will appear for almost tables
            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_apply');
    }
}
