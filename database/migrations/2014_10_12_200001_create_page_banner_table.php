<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageBannerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_banner', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('page_id')->unsigned()->index()->nullable();
            $table->foreign('page_id')->references('id')->on('pages');
            $table->integer('banner_id')->unsigned()->index()->nullable();
            $table->foreign('banner_id')->references('id')->on('banner');
           //The field that will appear for almost tables
            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_banner');
    }
}
