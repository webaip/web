<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('service_id')->unsigned()->index()->nullable();
            $table->integer('data_order')->index()->nullable();
            $table->string('position', 550)->nullable();
            $table->string('slug', 550)->nullable();
            $table->string('location', 550)->nullable();
            $table->text('description')->nullable();
            $table->text('requirment')->nullable();
            $table->string('image', 250)->nullable();
            $table->boolean('is_published')->nullable();
            $table->date('start_date')->nullable();
            $table->date('closed_date')->nullable();
           //The field that will appear for almost tables
            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
