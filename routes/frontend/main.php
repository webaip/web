<?php
Route::get('/home', 						[ 'as' => 'home',						'uses' => 'HomeController@index']);
Route::put('/submit-consultant', 			[ 'as' => 'submit-consultant',			'uses' => 'FrontendController@submitConsultant']);

Route::get('/about-us', 			        [ 'as' => 'about-us',					'uses' => 'AboutUsController@index']);
Route::get('/teams', 		                [ 'as' => 'teams',					    'uses' => 'TeamsController@index']);
Route::get('/expert', 		                [ 'as' => 'expert',					    'uses' => 'ExpertController@index']);
Route::get('/award', 		                [ 'as' => 'award',					    'uses' => 'AwardController@index']);

Route::get('/our-business/{biz}', 			[ 'as' => 'our-business',				'uses' => 'OurBusinessController@index']);

Route::get('/services', 					[ 'as' => 'services',					'uses' => 'ServicesController@index']);
Route::get('/matching', 		            [ 'as' => 'matching',					'uses' => 'MatchingController@index']);
Route::get('/customize', 		            [ 'as' => 'customize',					'uses' => 'CustomizeController@index']);
Route::get('/bussiness', 		            [ 'as' => 'bussiness',					'uses' => 'BussinessController@index']);

Route::get('/case', 					    [ 'as' => 'case',					    'uses' => 'CaseController@index']);

Route::get('/projects/{biz}', 				[ 'as' => 'projects',					'uses' => 'ProjectController@index']);
Route::get('/projects/{biz}/{slug}', 		[ 'as' => 'project-detail',				'uses' => 'ProjectController@detail']);

Route::get('/jobs/{biz}', 				    [ 'as' => 'jobs',						'uses' => 'JobController@index']);
Route::get('/jobs/{biz}/{slug}', 			[ 'as' => 'job-detail',					'uses' => 'JobController@detail']);

Route::get('/contact-us', 					[ 'as' => 'contact-us',					'uses' => 'ContactUsController@index']);
Route::put('/submit-contact', 			    [ 'as' => 'submit-contact',			    'uses' => 'ContactUsController@put']);

Route::get('/news/{biz}', 				    [ 'as' => 'news',					    'uses' => 'NewsController@index']);
Route::get('/news/{biz}/{slug}', 		    [ 'as' => 'news-detail',				'uses' => 'NewsController@detail']);


