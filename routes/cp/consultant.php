<?php

Route::get('/', 				['as' => 'index', 			'uses' => 'ConsultantController@index']);
Route::get('/create', 			['as' => 'create', 			'uses' => 'ConsultantController@create']);
Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'ConsultantController@edit']);
Route::post('/', 				['as' => 'update', 			'uses' => 'ConsultantController@update']);
Route::put('/', 				['as' => 'store', 			'uses' => 'ConsultantController@store']);
Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'ConsultantController@trash']);

