<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Our Bissinesse

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'OurBussinessController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'OurBussinessController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'OurBussinessController@update']);
});	