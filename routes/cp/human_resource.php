<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Our Bissinesse

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'HumanResourceController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'HumanResourceController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'HumanResourceController@update']);
});	