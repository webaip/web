<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'FeedbackController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'FeedbackController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'FeedbackController@update']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'FeedbackController@create']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'FeedbackController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'FeedbackController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'FeedbackController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'FeedbackController@updateStatus']);
});	