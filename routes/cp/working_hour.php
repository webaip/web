<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Our Bissinesse

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'WorkingHourController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'WorkingHourController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'WorkingHourController@update']);
});	