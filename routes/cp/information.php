<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'InformationController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'InformationController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'InformationController@update']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'InformationController@create']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'InformationController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'InformationController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'InformationController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'InformationController@updateStatus']);
});	