<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'JobController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'JobController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'JobController@update']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'JobController@create']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'JobController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'JobController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'JobController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'JobController@updateStatus']);
	Route::get('aaply/{id}', 		['as' => 'apply', 			'uses' => 'JobController@apply']);
});	