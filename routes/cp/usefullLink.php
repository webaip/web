<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'UsefullLinkController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'UsefullLinkController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'UsefullLinkController@update']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'UsefullLinkController@create']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'UsefullLinkController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'UsefullLinkController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'UsefullLinkController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'UsefullLinkController@updateStatus']);
});	