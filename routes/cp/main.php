<?php

	//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Auth
	Route::group(['as' => 'auth.', 'prefix' => 'auth', 'namespace' => 'Auth'], function(){	
		require(__DIR__.'/auth.php');
	});
	
	//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Authensicated
	Route::group(['middleware' => 'authenticatedUser'], function() {
		Route::group(['as' => 'user.',  'prefix' => 'user', 'namespace' => 'User'], function () {
			require(__DIR__.'/user.php');
		});
		
		Route::group(['as' => 'content.',  'prefix' => 'content', 'namespace' => 'Content'], function () {
			require(__DIR__.'/content.php');
		});
		Route::group(['as' => 'message.',  'prefix' => 'message', 'namespace' => 'Message'], function () {
			require(__DIR__.'/message.php');
		});
		Route::group(['as' => 'consultant.',  'prefix' => 'consultant', 'namespace' => 'Consultant'], function () {
			require(__DIR__.'/consultant.php');
		});
		Route::group(['as' => 'logo.',  'prefix' => 'logo', 'namespace' => 'Logo'], function () {
			require(__DIR__.'/logo.php');
		});
		Route::group(['as' => 'slide.',  'prefix' => 'slide', 'namespace' => 'Slide'], function () {
			require(__DIR__.'/slide.php');
		});
		Route::group(['as' => 'banner.',  'prefix' => 'banner', 'namespace' => 'Banner'], function () {
			require(__DIR__.'/banner.php');
		});
		
		Route::group(['as' => 'partner.',  'prefix' => 'partner', 'namespace' => 'Partner'], function () {
			require(__DIR__.'/partner.php');
		});
		Route::group(['as' => 'activity.',  'prefix' => 'activity', 'namespace' => 'Activity'], function () {
			require(__DIR__.'/activity.php');
		});
		
		Route::group(['as' => 'image.',  'prefix' => 'image', 'namespace' => 'Image'], function () {
			require(__DIR__.'/image.php');
		});
		
		Route::group(['as' => 'project.',  'prefix' => 'project', 'namespace' => 'Project'], function () {
			require(__DIR__.'/project.php');
		});

		Route::group(['as' => 'team.',  'prefix' => 'team', 'namespace' => 'Team'], function () {
			require(__DIR__.'/team.php');
		});
		Route::group(['as' => 'feedback.',  'prefix' => 'feedback', 'namespace' => 'Feedback'], function () {
			require(__DIR__.'/feedback.php');
		});
		Route::group(['as' => 'news.',  'prefix' => 'news', 'namespace' => 'News'], function () {
			require(__DIR__.'/news.php');
		});
		Route::group(['as' => 'job.',  'prefix' => 'job', 'namespace' => 'Job'], function () {
			require(__DIR__.'/job.php');
		});
		Route::group(['as' => 'our_bussiness.',  'prefix' => 'our_bussiness', 'namespace' => 'OurBussiness'], function () {
			require(__DIR__.'/our_bussiness.php');
		});
		Route::group(['as' => 'about_us.',  'prefix' => 'about_us', 'namespace' => 'AboutUs'], function () {
			require(__DIR__.'/about_us.php');
		});
		Route::group(['as' => 'information.',  'prefix' => 'information', 'namespace' => 'Information'], function () {
			require(__DIR__.'/information.php');
		});
		Route::group(['as' => 'consultations_content.',  'prefix' => 'consultations_content', 'namespace' => 'ConsultationsContent'], function () {
			require(__DIR__.'/consultations_content.php');
		});
		Route::group(['as' => 'company_profile.',  'prefix' => 'company_profile', 'namespace' => 'CompanyProfile'], function () {
			require(__DIR__.'/company_profile.php');
		});

		Route::group(['as' => 'contact_address.',  'prefix' => 'contact_address', 'namespace' => 'ContactAddress'], function () {
			require(__DIR__.'/contact_address.php');
		});
		Route::group(['as' => 'human_resource.',  'prefix' => 'human_resource', 'namespace' => 'HumanResource'], function () {
			require(__DIR__.'/human_resource.php');
		});
		Route::group(['as' => 'working_hour.',  'prefix' => 'working_hour', 'namespace' => 'WorkingHour'], function () {
			require(__DIR__.'/working_hour.php');
		});
		Route::group(['as' => 'usefullLink.',  'prefix' => 'usefullLink', 'namespace' => 'UsefullLink'], function () {
			require(__DIR__.'/usefullLink.php');
		});
	});