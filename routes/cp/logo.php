<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'LogoController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'LogoController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'LogoController@update']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'LogoController@create']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'LogoController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'LogoController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'LogoController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'LogoController@updateStatus']);
});	

