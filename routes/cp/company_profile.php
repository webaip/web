<?php

Route::get('/', 				['as' => 'index', 			'uses' => 'CompanyProfileController@index']);
Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'CompanyProfileController@showEditForm']);
Route::post('/', 				['as' => 'update', 			'uses' => 'CompanyProfileController@update']);

