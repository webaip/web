<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Our Bissinesse

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'ContactAddressController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'ContactAddressController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'ContactAddressController@update']);
});	